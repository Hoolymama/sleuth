

#ifndef curveChainMesh_H
#define curveChainMesh_H


#include <vector>

#include <maya/MPointArray.h>
#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MStatus.h>
#include <maya/MVector.h>


class curveChainMesh : public MPxNode
{
public:
	curveChainMesh();
	virtual           ~curveChainMesh();

	virtual MStatus   compute( const MPlug&, MDataBlock& );
	virtual void      postConstructor();

	static  void *    creator();
	static  MStatus   initialize();
	static  MTypeId   id;


private:

	static MObject aPosition;
	// static MObject aVValues;

	static MObject aUOrder;
	static MObject aVOrder;


	// static MObject aNumUVerts;
    static MObject aClosedCurve;
    static MObject aFixStart;

    static MObject  aNumUVerts;
    static MObject  aNumVVerts;


	static MObject aOutMesh;





 MStatus  generateTubeUVs(
 	int numUVerts,
    int numVVerts,
    MFloatArray & uVals,
    MFloatArray & vVals,
	float vMax=1.0f) ;

 MStatus  generateRibbonUVs(
 	int numUVerts,
    int numVVerts,
    MFloatArray & uVals,
    MFloatArray & vVals,
	float vMax=1.0f
) ;

void createTube(
    int numUVerts,
    int numVVerts,
    MIntArray & faceCounts,
    MIntArray & connectivity
)  ;

void createRibbon(
    int numUVerts,
    int numVVerts,
    MIntArray & faceCounts,
    MIntArray & connectivity
)  ;


 void  createTubeUVMapping(
 	int numUVerts,
    int numVVerts,
    MIntArray & uvCounts,
    MIntArray & uvIds)  ;

 void  createRibbonUVMapping( 	
 	int numUVerts,
    int numVVerts,
    MIntArray & uvCounts,
    MIntArray & uvIds) ;




};
#endif



