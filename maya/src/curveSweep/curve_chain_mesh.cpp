
#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MPxNode.h>
#include <maya/MStatus.h>

#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPluginData.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnMeshData.h>
#include <maya/MFloatPoint.h>
#include <maya/MFnMesh.h>
#include <maya/MTime.h>

#include <mayaMath.h>
#include "errorMacros.h"
#include "jMayaIds.h"

#include <profile_chain_data.h>


#include "curve_chain_mesh.h"


#include <vert.h>



MObject curveChainMesh::aPosition;

MObject curveChainMesh::aUOrder;
MObject curveChainMesh::aVOrder;

MObject curveChainMesh::aNumUVerts;

MObject curveChainMesh::aNumVVerts;

MObject curveChainMesh::aClosedCurve;

MObject curveChainMesh::aFixStart;
MObject curveChainMesh::aOutMesh;


const float fTol = 0.0000001;
const double PI_X2 = 6.283185306;
const double PI = 3.141592653;

MTypeId curveChainMesh::id( k_curveChainMesh );

curveChainMesh::curveChainMesh() {	}

curveChainMesh::~curveChainMesh()
{
}

void* curveChainMesh::creator()
{
	return new curveChainMesh();
}
void curveChainMesh::postConstructor( ){}


MStatus curveChainMesh::initialize()
{
	MStatus st;

	MString method("curveChainMesh::initialize");
	MFnNumericAttribute nAttr; 
	MFnTypedAttribute tAttr; 
	MFnCompoundAttribute cAttr; 
	MFnUnitAttribute uAttr;

	aPosition = tAttr.create("position", "pos", MFnData::kVectorArray);
    tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setCached(false);
	st = addAttribute(aPosition);mser;


	aUOrder = tAttr.create("uOrder", "uord", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aUOrder);mser;

	aVOrder = tAttr.create("vOrder", "vord", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aVOrder);mser;



	aClosedCurve = nAttr.create( "closedCurve", "cc",MFnNumericData::kBoolean );
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(false);
	st =  addAttribute(aClosedCurve);  mser;


	aFixStart = nAttr.create( "fixStart", "fs",MFnNumericData::kBoolean );
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(false);
	st =  addAttribute(aFixStart);  mser;

	aNumUVerts = nAttr.create( "numUVerts", "nu",MFnNumericData::kLong );
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(2);
	st =  addAttribute(aNumUVerts);  mser;

	aNumVVerts = nAttr.create( "numVVerts", "nv",MFnNumericData::kLong );
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(2);
	st =  addAttribute(aNumVVerts);  mser;



	aOutMesh = tAttr.create( "outMesh", "om", MFnData::kMesh );
	tAttr.setStorable(false);
	tAttr.setWritable(false);
	addAttribute( aOutMesh );

	attributeAffects (aPosition, aOutMesh);

	attributeAffects (aUOrder, aOutMesh);
	attributeAffects (aVOrder, aOutMesh);

	attributeAffects (aNumUVerts, aOutMesh);
	attributeAffects (aNumVVerts, aOutMesh);

	// attributeAffects (aUValues, aOutMesh);
	// attributeAffects (aVValues, aOutMesh);
	// attributeAffects (aNumUVerts, aOutMesh);
	attributeAffects (aClosedCurve, aOutMesh);
	attributeAffects (aFixStart, aOutMesh);

	return MS::kSuccess;
}

 MStatus  curveChainMesh::generateTubeUVs(
 	int numUVerts,
    int numVVerts,
    MFloatArray & uVals,
    MFloatArray & vVals,
	float vMax)  
 {
 	if (vMax < fTol ) vMax = 1.0;
 	if (numUVerts < 2) return MS::kUnknownParameter;
 	if (numVVerts < 2) return MS::kUnknownParameter;

 	MFloatArray tmpU((numUVerts+1));
 	MFloatArray tmpV(numVVerts);


 	for (int i = 0;i<(numUVerts+1);i++) {
 		tmpU.set((float(i) / float(numUVerts) ), i);
 	}

	for (int i = 0;i< numVVerts;i++) {
		tmpV.set((float(i) / float((numVVerts-1) * vMax) ), i);
	}

 	for (int j = 0;j<(numVVerts);j++) {  
        for (int k = 0;k<(numUVerts+1);k++) {
        	uVals.append(tmpU[k]);
        	vVals.append(tmpV[j]);
        }
    }
    return MS::kSuccess;
 }

 MStatus  curveChainMesh::generateRibbonUVs(
 	int numUVerts,
    int numVVerts,
    MFloatArray & uVals,
    MFloatArray & vVals,
	float vMax
)   {   

 	if (vMax < fTol ) vMax = 1.0;
 	if (numUVerts < 2) return MS::kUnknownParameter;
 	if (numVVerts < 2) return MS::kUnknownParameter;

	MFloatArray tmpU((numUVerts));
 	MFloatArray tmpV(numVVerts);

 	for (int i = 0;i<(numUVerts);i++) {
 		tmpU.set((i / float(numUVerts-1) ), i);
 	}
	for (int i = 0;i< numVVerts;i++) {
		tmpV.set((i / float((numVVerts-1) * vMax) ), i);
	}

 	for (int j = 0;j<(numVVerts);j++) {  
        for (int k = 0;k<(numUVerts);k++) {
        	uVals.append(tmpU[k]);
        	vVals.append(tmpV[j]);
        }
    }
    return MS::kSuccess;
}


void curveChainMesh::createTube(
    int numUVerts,
    int numVVerts,
    MIntArray & faceCounts,
    MIntArray & connectivity
)  
{

	int numFaces =   (numUVerts * (numVVerts-1)) ;
	faceCounts = MIntArray(numFaces, 4);
	connectivity.setLength(0);
	int vertexIndex=0;
    for (int j = 0;j<(numVVerts-1);j++) {
     
        for (int k = 0;k<(numUVerts-1);k++) {
            connectivity.append(vertexIndex);
            connectivity.append(vertexIndex+1);
            connectivity.append(vertexIndex+numUVerts+1);
            connectivity.append(vertexIndex+numUVerts);
            vertexIndex+=1;
        }
        connectivity.append(vertexIndex);
        connectivity.append((vertexIndex+1) - numUVerts);
        connectivity.append(vertexIndex+1);
        connectivity.append(vertexIndex+numUVerts);
        vertexIndex +=1; // jump to next V
    }
}


void curveChainMesh::createRibbon(
    int numUVerts,
    int numVVerts,
    MIntArray & faceCounts,
    MIntArray & connectivity
)  
{

	int numFaces =   ((numUVerts-1) * (numVVerts-1)) ;
	faceCounts = MIntArray(numFaces, 4);
	connectivity.setLength(0);

	int vertexIndex=0;
    for (int j = 0;j<(numVVerts-1);j++) {
     
        for (int k = 0;k<(numUVerts-1);k++) {
            connectivity.append(vertexIndex);
            connectivity.append(vertexIndex+1);
            connectivity.append(vertexIndex+numUVerts+1);
            connectivity.append(vertexIndex+numUVerts);
            vertexIndex+=1;
        }
        vertexIndex+=1; // jump to next V
    }
}


 void  curveChainMesh::createTubeUVMapping(
 	int numUVerts,
    int numVVerts,
    MIntArray & uvCounts,
    MIntArray & uvIds)  
 {

 	int uvIndex = 0;

    for (int j = 0;j<(numVVerts-1);j++) {
 
        for (int k = 0;k<(numUVerts);k++) {
            uvCounts.append(4);
            uvIds.append(uvIndex);
            uvIds.append(uvIndex+1);
            uvIds.append(uvIndex+numUVerts+2);
            uvIds.append(uvIndex+numUVerts+1);

            uvIndex+=1; // work our way round.
        }
        uvIndex+=1; // jump up one step.
    }
 }


 void  curveChainMesh::createRibbonUVMapping( 	
 	int numUVerts,
    int numVVerts,
    MIntArray & uvCounts,
    MIntArray & uvIds)  
 {

 	int uvIndex = 0;

    for (int j = 0;j<(numVVerts-1);j++) {
 
        for (int k = 0;k<(numUVerts-1);k++) {
            uvCounts.append(4);
            uvIds.append(uvIndex);
            uvIds.append(uvIndex+1);
            uvIds.append(uvIndex+numUVerts+1);
            uvIds.append(uvIndex+numUVerts);

            uvIndex+=1; // work our way along.
        }
        uvIndex+=1; // jump up one step.
    }
 }






MStatus curveChainMesh::compute( const MPlug& plug,  MDataBlock& data ) 
{ 
	MStatus st;
// JPMDBG;
	
	MString method("curveChainMesh::compute");

	if(plug != aOutMesh) return MS::kUnknownParameter;


	int numUVerts = data.inputValue(aNumUVerts).asInt();
	int numVVerts = data.inputValue(aNumVVerts).asInt();

	if ((numUVerts < 2) || (numVVerts < 2)) return MS::kUnknownParameter; 

	unsigned vl = numUVerts * numVVerts;

	bool closedCurve = data.inputValue(aClosedCurve).asBool();
	bool fixStart = data.inputValue(aFixStart).asBool();

	MIntArray faceCounts;
	MIntArray connectivity;
	MIntArray   uvCounts;
	MIntArray   uvIds;
	MFloatArray uVals;
	MFloatArray vVals;
// JPMDBG;
	bool generateUVs = true;
	if (closedCurve) {
		createTube( numUVerts, numVVerts, faceCounts, connectivity);
		createTubeUVMapping(numUVerts, numVVerts, uvCounts, uvIds);
		st = generateTubeUVs(numUVerts, numVVerts, uVals, vVals);
		if (st.error()) generateUVs = false;
	} else {
		createRibbon( numUVerts, numVVerts, faceCounts, connectivity);
		createRibbonUVMapping(numUVerts, numVVerts, uvCounts, uvIds);
		st = generateRibbonUVs(numUVerts, numVVerts, uVals, vVals);
		if (st.error()) generateUVs = false;
	}


	MFnMeshData fnC;
	MObject outGeom = fnC.create(&st);mser;
	MDataHandle hMesh = data.outputValue(aOutMesh, &st);


	MPointArray vertices(vl) ;	
  	MFnMesh fnMesh;
    fnMesh.create( 
        vl, 
        faceCounts.length(), 
        vertices, 
        faceCounts, 
        connectivity, outGeom, &st 
    );  mser;

    if (generateUVs) {
    	st = fnMesh.setUVs(uVals, vVals);  mser; 
    	st = fnMesh.assignUVs ( uvCounts, uvIds ); mser; 
	}
	
	MFnVectorArrayData fnV;
	MFnDoubleArrayData fnD;
	MDataHandle h;
	MObject d;
// JPMDBG;
	
	unsigned pl = 0;

	h = data.inputValue(aPosition, &st);msert;
	d = h.data();
	st = fnV.setObject(d);msert;
	pl = fnV.length();

	const MVectorArray & pos = fnV.array();	

	if ((int(pl) < (numUVerts*2)) || (( int(pl) % numUVerts)!=0)) {
		hMesh.set(outGeom);data.setClean(aOutMesh);return MS::kSuccess;
	}	

	bool doSort =true;
	h = data.inputValue(aUOrder, &st);msert;
	d = h.data();
	st = fnD.setObject(d);msert;
	if (fnD.length() != pl) {doSort = false;}
	const MDoubleArray & uord = fnD.array();	

	
	h = data.inputValue(aVOrder, &st);msert;
	d = h.data();
	st = fnD.setObject(d);msert;
	if (fnD.length() != pl) {doSort = false;}
	const MDoubleArray & vord = fnD.array();	



	// sort the verts on V value then U Value
	// we do this just in case - although they should already be sorted.
	VERT_ARRAY sverts;
	for (unsigned i = 0; i < pl; ++i)
	{
		vert tmpVert;
		tmpVert.position =  pos[i];
		if (doSort) {
			tmpVert.u =  float(uord[i]) ;
			tmpVert.v =  float(vord[i]);
		}
		sverts.push_back(tmpVert);
	}


    if (doSort) std::sort(sverts.begin(), sverts.end(), vert_compare);


    if (pl < vl) {
        // cerr << "adding because pl < vl: " << pl << " - " << vl << endl;
	
    	int lastRingStart = int(pl) - numUVerts;
    	// int lastRingEnd = pl;
    	// cerr << "lastRingStart: " << lastRingStart << endl;
    	for (unsigned i = pl; i < vl; i++) {
    		int index = (i % numUVerts) + lastRingStart;
    		// cerr << "i: " << i << " - index: " << index << endl;
    		vert tmpVert;
    		tmpVert.position =  pos[index];
    		tmpVert.u =  0.0f ;
    		tmpVert.v =  0.0f;
    		sverts.push_back(tmpVert);
    	}
    }
    // if (sverts.size() != vl) {

    // 	cerr << "sverts.size() != vl: " << sverts.size() << " - " << vl << endl;
    // } 


// JPMDBG;

    // count u values while they are increasing to find out how many in U
    // when a u value decreases, we know thats a whole row
  //   int numUVerts;
  //   int peg = -1.0;
  //   for (int i = 0; i < pl; ++i)
  //   {
  //   	if (sverts[i].u < peg) {
  //   		numUVerts = i;
  //   		break;
  //   	}
  //   	peg = sverts[i].u;
  //   }
 	// if (numUVerts < 2) return MS::kUnknownParameter;	
  //   // should divide exactly
  //   if (pl % numUVerts) return MS::kUnknownParameter;	


// JPMDBG;
		// int numVVerts = pl / numUVerts;
	// JPMDBG;
	
    // should be at least 4 verts
	// if (numVVerts < 2)  return MS::kUnknownParameter;	




// JPMDBG;
	// int numFaces =  (closedCurve) ? (numUVerts * numVVerts-1)  : (numUVerts-1 * numVVerts-1);

    // verts are now in the right order to construct a mesh.

    // if more points than verts in mesh,
    // use the last ones made (for now)
    int offset = 0;
    if ((pl > vl) && (fixStart)) offset = pl - vl;

	for (unsigned i = 0; i < vl; ++i) {
		vertices.set(MPoint(sverts[(i+offset)].position),i);
	}
	fnMesh.setPoints(vertices);


// JPMDBG;
	

	
//   	MFnMesh fnMesh;
//     fnMesh.create( 
//         vertices.length(), 
//         faceCounts.length(), 
//         vertices, 
//         faceCounts, 
//         connectivity, outGeom, &st 
//     );  mser;

// // JPMDBG;
//     if (generateUVs) {
//     	st = fnMesh.setUVs(uVals, vVals);  mser; 
//     	st = fnMesh.assignUVs ( uvCounts, uvIds ); mser; 
// 	}

// JPMDBG;
	

	//MDataHandle hMesh = data.outputValue(aOutMesh, &st);
	hMesh.set(outGeom);
	data.setClean(aOutMesh);
// JPMDBG;
	
	return MS::kSuccess;
}