

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDataHandle.h>
// #include <maya/MFnDynSweptGeometryData.h>
// #include <maya/MDynSweptLine.h> 

#include <maya/MPlugArray.h>
#include <maya/MRenderUtil.h> 
#include <maya/MQuaternion.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MArrayDataBuilder.h>



#include "mayaFX.h"
#include "mayaMath.h"
#include "attrUtils.h"
#include "jMayaIds.h"
#include "curve_chain_emitter.h"

MObject curveChainEmitter::aUserVectorPP;
MObject curveChainEmitter::aPoints0;
MObject curveChainEmitter::aPoints1;
MObject curveChainEmitter::aUValues;
// MObject curveChainEmitter::aClosedCurve;

MObject curveChainEmitter::aInheritVelocity;
MObject curveChainEmitter::aInheritVelocityRamp;

MObject curveChainEmitter::aVectorSpeed;
MObject curveChainEmitter::aVectorSpeedRamp;

// MObject curveChainEmitter::aSweepTimeOffset;
const double  PI  = 3.1415927;
const double  TAU = 2.0 * PI;


MTypeId curveChainEmitter::id( k_curveChainEmitter );


curveChainEmitter::curveChainEmitter()
{
}

curveChainEmitter::~curveChainEmitter()
{
}

void *curveChainEmitter::creator()
{
	return new curveChainEmitter;
}




unsigned curveChainEmitter::emitFromPoint(
	double dt,
	int rate,
	int vertexId,
	const MVector &point,
	const MVector &velocity,
	const MVector &userVector,
	float inheritedVelocity,
	float vectorSpeed ,
	double speed,
	const MVector &dirV,
	MVectorArray &outPos,
	MVectorArray &outVel,
	MDoubleArray &outTime, // time in step
	MDoubleArray &outVertex
) 
{
	// loop from t=1 to t=rate
	MVector emissionPoint;
	MVector diff = velocity*dt;
	MVector lastPoint = point - diff;

	MVector emissionVel = dirV;
	emissionVel += (velocity * inheritedVelocity);
	emissionVel += (userVector * vectorSpeed);
	emissionVel *= speed;


	for (int i = 1; i <= rate; ++i)
	{
		double sweep = i / double(rate);
		emissionPoint= lastPoint + (diff*sweep);





//cerr << "dt: " << dt <<endl;
//cerr << "sweep: " << sweep <<endl;
//cerr << "emissionPoint: " << emissionPoint <<endl;

		emissionPoint = emissionPoint - (emissionVel *dt* sweep);
//cerr << "emissionPoint: " << emissionPoint <<endl;
//	cerr << "-------------------------------------" << endl;
		outPos.append(emissionPoint) ;		
		outVel.append(emissionVel);
		// outTime.append(sweep);
		outTime.append(sweep);
		
		outVertex.append(vertexId);

	}
	return unsigned(rate);
}

MStatus curveChainEmitter::initialize()

{
	MStatus st;
//  unsigned int counter = 0;
	MString method("curveChainEmitter::initialize");

	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;
	MFnEnumAttribute eAttr;
	MFnNumericAttribute nAttr;


	aPoints0 = tAttr.create("positions0", "pos0", MFnData::kVectorArray, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aPoints0);	mser;

	aPoints1 = tAttr.create("positions1", "pos1", MFnData::kVectorArray, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aPoints1);	mser;

	aUValues = tAttr.create("uValues", "us", MFnData::kDoubleArray, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aUValues);	mser;


	aInheritVelocity =  nAttr.create( "inheritVelocity", "ivel", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritVelocity);	mser;

	aInheritVelocityRamp = MRampAttribute::createCurveRamp("inheritVelocityRamp","ivrmp");
	st = addAttribute( aInheritVelocityRamp );mser;


	aVectorSpeed =  nAttr.create( "userVectorSpeed", "vsp", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aVectorSpeed);	mser;

	aVectorSpeedRamp = MRampAttribute::createCurveRamp("userVectorSpeedRamp","vsrmp");
	st = addAttribute( aVectorSpeedRamp );mser;

	aUserVectorPP = tAttr.create("userVectorPP", "vcpp", MFnData::kVectorArray, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st =	addAttribute(aUserVectorPP);	mser;


	return( MS::kSuccess );
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
MStatus curveChainEmitter::doRampLookup( 
	const MObject& attribute, 
	const MDoubleArray& in, 
	float mult, 
	MFloatArray& results ) const
{
	MStatus st;
	MRampAttribute rampAttr( thisMObject(), attribute, &st ); msert;
	MIntArray ids;
	MFloatArray positions;
	MFloatArray values;
	MIntArray interps;
	// if the ramp is full of 1s then output the mult in each element.
	rampAttr.getEntries(ids,positions,values,interps);
	bool isConstantOne = true;
	for (int i = 0; i < values.length(); ++i)
	{
		if (values[i] != 1.0) {
			isConstantOne = false;
			break;
		}
	}
	unsigned n = in.length();
	results.setLength(n);

	if (isConstantOne) {
		for( unsigned i = 0; i < n; i++ ) {
			results[i] = mult;
		}
	} else {
		for( unsigned i = 0; i < n; i++ )
		{
			float & value = results[i];
			rampAttr.getValueAtPosition( float(in[i]), value, &st ); mser;
			value *= mult;
		}
	}
	return MS::kSuccess;
}



MStatus curveChainEmitter::compute(const MPlug& plug, MDataBlock& data)
{

	MStatus st;
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );
	
		// has no effect
	if ( data.inputValue( state ).asShort() == 1 ) return MS::kSuccess;


	MObject thisNode = thisMObject();
	MFnDependencyNode thisNodeFn(thisNode);

	int multiIndex = plug.logicalIndex( &st);mser;
	MArrayDataHandle hOutArray = data.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &st );mser;


	// position and velocity arrays to append new particle data.
	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MDoubleArray outTime = fnOutput.doubleArray("timeInStep", &st);mser;
	MDoubleArray outVertex = fnOutput.doubleArray("vertexId", &st);mser;


	// check time and isFull
	bool beenFull = isFullValue( multiIndex, data );
	if( beenFull ) return( MS::kSuccess );
	MTime cT = currentTimeValue( data );
	MTime sT = startTimeValue( multiIndex, data );
	MTime dT = deltaTimeValue( multiIndex, data );
	double dt =  dT.as(MTime::kSeconds);
	double dtRecip = 1.0 / dt;

	if( (cT <= sT) || (dT <= 0.0) ){
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}


	bool validInput = true;
	MDataHandle h;
	MObject d;
	MFnVectorArrayData fnV;
	MFnDoubleArrayData fnD;

	h = data.inputValue( aPoints1 , &st); msert;
	d = h.data();
	st = fnV.setObject(d); msert;
	MVectorArray points1 = fnV.array( &st );msert;	
	unsigned pl = points1.length();
	if (!pl) validInput = false;


	h = data.inputValue( aPoints0, &st); msert;
	d = h.data();
	st = fnV.setObject(d);msert;
	MVectorArray points0 = fnV.array( &st );msert;
	if (pl != points0.length()) validInput = false;


	h = data.inputValue( aUValues , &st); msert;
	d = h.data();
	st = fnD.setObject(d); msert;
	MDoubleArray uValues = fnD.array( &st ); msert;
	if (pl != uValues.length()) validInput = false;


	if (! validInput ) {
		hOut.set( dOutput );
		data.setClean( plug );
		return( MS::kSuccess );
	}

	MVectorArray velocities(pl);
	for (int i = 0; i < pl; ++i)
	{
		velocities.set( ((points1[i] - points0[i]) * dtRecip), i);
	}

	bool hasUserVectorPP = true;
	h = data.inputValue( aUserVectorPP );
	d = h.data();
	st = fnV.setObject(d);
	if (st.error()) hasUserVectorPP = false;

	MVectorArray userVectorPP ;
	if (hasUserVectorPP) {
		if (fnV.length() == pl) {
			userVectorPP.copy(fnV.array()) ;
		} else {
			hasUserVectorPP = false;
		}
	}	

	if (! hasUserVectorPP) {
		userVectorPP = MVectorArray(pl);
	}

	double rate =  (int)data.inputValue( mRate ).asDouble() ;
	// cerr << "rate" << rate << endl;
	// int rate  = int(rate);
	if (rate > 12) rate = 12;


	double speed = doubleValue( data, mSpeed );

	double inheritVelocity = doubleValue( data, aInheritVelocity );
	double vectorSpeed = doubleValue( data, aVectorSpeed );
	MVector dirV = vectorValue( data, mDirection );

	MFloatArray inheritedVelocities, vectorSpeeds;

	doRampLookup( curveChainEmitter::aInheritVelocityRamp, uValues, inheritVelocity, inheritedVelocities);
	doRampLookup( curveChainEmitter::aVectorSpeedRamp, uValues, vectorSpeed, vectorSpeeds);


	
	if (rate > 0){
		for (unsigned i = 0; i < pl; ++i)
		{
			emitFromPoint(
				dt,
				rate,
				i,
				points1[i],
				velocities[i],
				userVectorPP[i] ,
				inheritedVelocities[i] ,
				vectorSpeeds[i] ,
				speed,dirV,
				outPos,
				outVel,
				outTime,
				outVertex
				);
		}
	}
	hOut.set( dOutput );
	data.setClean( plug );
	return( MS::kSuccess );
}


