
#include <maya/MIOStream.h>
#include <maya/MTime.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MPxEmitterNode.h>
#include <maya/MFloatVectorArray.h>

#include "lookup.h"
#include "errorMacros.h"


class curveChainEmitter: public MPxEmitterNode
{
public:
	curveChainEmitter();
	virtual ~curveChainEmitter();
	
	static void		*creator();
	static MStatus	initialize();
	virtual MStatus	compute( const MPlug& plug, MDataBlock& block );



	static MObject aPoints0;
	static MObject aPoints1;
	static MObject aUValues;

	static MObject aUserVectorPP;
	
	// static MObject aClosedCurve;

	static MObject aInheritVelocity;
	static MObject aInheritVelocityRamp;

	static MObject aVectorSpeed;
	static MObject aVectorSpeedRamp;

	static MTypeId	id;
	

private:
MStatus doRampLookup( 
	const MObject& attribute, 
	const MDoubleArray& in, 
	float mult, 
	MFloatArray& results ) const;



unsigned emitFromPoint(
	double dt,
	int rate,
	int i,
	const MVector &point,
	const MVector &velocity,
	const MVector &userVector,
	float inheritedVelocity,
	float vectorSpeed ,
	double speed,
	const MVector &dirV,
	MVectorArray &outPos,
	MVectorArray &outVel,
	MDoubleArray &outTime,
	MDoubleArray &outVertex
) ;
	double	doubleValue( MDataBlock& block, MObject &att );
	MVector	vectorValue( MDataBlock& block, MObject &att );
	bool	isFullValue( int plugIndex, MDataBlock& block );
	long	seedValue( int plugIndex, MDataBlock& block );
	double	inheritFactorValue( int plugIndex, MDataBlock& block );

	MTime	currentTimeValue( MDataBlock& block );
	MTime	startTimeValue( int plugIndex, MDataBlock& block );
	MTime	deltaTimeValue( int plugIndex, MDataBlock& block );

	// for calculating triangle acceleration
	// MVectorArray m_lastVelocity;
	// values to interpolate
	// MDoubleArray m_lastEmissionRate;
	// MVectorArray m_lastEmissionVec;	
	// unsigned m_lastTriangleCount;
};

// inlines
//
inline double curveChainEmitter::doubleValue( MDataBlock& block , MObject &att)
{
	MStatus status;

	MDataHandle hValue = block.inputValue( att, &status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


inline MVector curveChainEmitter::vectorValue( MDataBlock& block,MObject &att )
{
	MStatus status;
	MVector valueV(0.0, 0.0, 0.0);

	MDataHandle hValue = block.inputValue( att, &status );

	if( status == MS::kSuccess )
	{
		double3 &value = hValue.asDouble3();

		valueV[0] = value[0];
		valueV[1] = value[1];
		valueV[2] = value[2];
	}

	return(valueV );
}

inline bool curveChainEmitter::isFullValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	bool value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mIsFull, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asBool();
		}
	}

	return( value );
}
inline long curveChainEmitter::seedValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	long value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mSeed, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asInt();
				//cerr << value << endl;
		}
	}

	return( value );
}

inline double curveChainEmitter::inheritFactorValue(int plugIndex,MDataBlock& block)
{
	MStatus status;
	double value = 0.0;

	MArrayDataHandle mhValue = block.inputArrayValue( mInheritFactor, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asDouble();
		}
	}

	return( value );
}

inline MTime curveChainEmitter::currentTimeValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mCurrentTime, &status );

	MTime value(0.0);
	if( status == MS::kSuccess )
		value = hValue.asTime();

	return( value );
}

inline MTime curveChainEmitter::startTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mStartTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}

	return( value );
}

inline MTime curveChainEmitter::deltaTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mDeltaTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}
	return( value );
}

