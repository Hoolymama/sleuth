
#ifndef vertStruct_H
#define vertStruct_H


#include <vector>


struct vert {
    MVector position;
	float u; 
	float v; 
};

typedef std::vector<vert> VERT_ARRAY;

struct comparison_struct {
    bool operator() (vert a, vert  b) { 
        if (a.v < b.v ) return true;
        if (b.v < a.v ) return false;

        if (a.u < b.u ) return true;
        if (b.u < a.u ) return false;

        return false;
    }
} vert_compare;

#endif

