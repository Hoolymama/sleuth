/*
 *  curveSampleData.cpp
 *  feather
 *
 *  Created by Julian Mann on 26/07/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */



#include <maya/MIOStream.h>

#include <math.h>

#include <maya/MDoubleArray.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MString.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>


#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MFnEnumAttribute.h> 
#include <maya/MDynSweptLine.h> 

#include <curveSampleData.h>
#include "jMayaIds.h"


#include <errorMacros.h>
#include <mayaMath.h>
//#include <glyph.h>



#define ONE_THIRD 0.333333333333


MObject curveSampleData::aSweptCurve;
MObject curveSampleData::aClosedCurve;
MObject curveSampleData::aPositions0;	
MObject curveSampleData::aPositions1;	
MObject curveSampleData::aUValues;
MObject curveSampleData::aMasses;
MObject curveSampleData::aDeltaTime;
MObject curveSampleData::aCount;

MTypeId curveSampleData::id( k_curveSampleData );

const double epsilon = 0.0001;

curveSampleData::curveSampleData() {}

curveSampleData::~curveSampleData() {}

void* curveSampleData::creator(){
	return new curveSampleData();
}

MStatus curveSampleData::initialize()
{
	MStatus st;
	
	MString method("curveSampleData::initialize");
	
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnEnumAttribute eAttr;

	aSweptCurve = tAttr.create( "sweptCurve", "swc", MFnData::kDynSweptGeometry); mser;
	tAttr.setReadable(false);
	tAttr.setStorable(false);
	addAttribute(aSweptCurve);

	aClosedCurve = nAttr.create( "closedCurve", "clc", MFnNumericData::kBoolean); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(false);
	addAttribute(aClosedCurve);

	aCount = nAttr.create( "count", "cnt", MFnNumericData::kLong); mser;
	nAttr.setReadable(true);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	addAttribute(aCount);

	aPositions1 = tAttr.create("positions1", "pos1", MFnData::kVectorArray, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aPositions1 );mser;

	aPositions0 = tAttr.create("positions0", "pos0", MFnData::kVectorArray, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aPositions0 );mser;

	aUValues = tAttr.create("uValues", "u", MFnData::kDoubleArray, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aUValues );mser;

	aMasses = tAttr.create("masses", "mas", MFnData::kDoubleArray, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aMasses );mser;


	st =attributeAffects(aSweptCurve,aPositions1 );mser;
	st =attributeAffects(aSweptCurve,aPositions0 );mser;
	st =attributeAffects(aSweptCurve,aUValues );mser;
	st =attributeAffects(aSweptCurve,aMasses );mser;
	st =attributeAffects(aSweptCurve,aCount );mser;

	st =attributeAffects(aClosedCurve,aPositions1 );mser;
	st =attributeAffects(aClosedCurve,aPositions0 );mser;
	st =attributeAffects(aClosedCurve,aUValues );mser;
	st =attributeAffects(aClosedCurve,aMasses );mser;
	st =attributeAffects(aClosedCurve,aCount );mser;



	return( MS::kSuccess );   
}


MStatus curveSampleData::compute( const MPlug& plug, MDataBlock& data )
{   
	MStatus st;
	MString method("curveSampleData::compute");
	
	if(!(
		(plug == aPositions0) || 
		(plug == aPositions1) ||
		(plug == aUValues) 
	)) return( MS::kUnknownParameter );
	// JPMDBG;
	MDataHandle sweptHandle = data.inputValue( aSweptCurve , &st );mser;
	MObject sweptData = sweptHandle.data();
	MFnDynSweptGeometryData fnSweptData( sweptData , &st );mser;
	// JPMDBG;
	
	bool closedCurve = data.inputValue(aClosedCurve).asBool();
	unsigned nLines = fnSweptData.lineCount();
	if (nLines < 1) return( MS::kUnknownParameter );

	unsigned nPoints = closedCurve ? nLines : (nLines + 1) ;

	// JPMDBG;

	MVectorArray positions1(nPoints);	
	MVectorArray positions0(nPoints);	
	MDoubleArray uValues(nPoints);	
	MDoubleArray masses(nPoints, 1.0);	
		// JPMDBG;
	
	MVector p0, p1;
	double totalLength = 0.0;
	unsigned j, k;
	
	unsigned idx = 0;
	// JPMDBG;

	while (idx < nLines) {
		MDynSweptLine line = fnSweptData.sweptLine(idx);
		positions0.set(line.vertex(0,0),idx);
		positions1.set(line.vertex(0,1),idx);
		uValues.set(totalLength,idx);
		totalLength += line.length();
		idx++;
	}
	

	// if not closed curve then we need to put the last point 
	// on, because I asume (should check though) that lines will join up
	// anyway if curve is closed.
	if (! closedCurve) {
		MDynSweptLine line = fnSweptData.sweptLine(idx-1); 
		positions0.set(line.vertex(1,0),idx);
		positions1.set(line.vertex(1,1),idx);
		uValues.set(totalLength,idx);
	}

	double lengthRecip = 1.0 / totalLength;
	for (int i = 0; i < nPoints; ++i)
	{
		uValues[i] = uValues[i] * lengthRecip;
	}

	MDataHandle hPositions0 = data.outputValue(aPositions0);
	MFnVectorArrayData fnPositions0;
	MObject dPositions0 = fnPositions0.create(positions0);
	hPositions0.set(dPositions0);
	st = data.setClean( aPositions0 );mser;
	// JPMDBG;

	MDataHandle hPositions1 = data.outputValue(aPositions1);
	MFnVectorArrayData fnPositions1;
	MObject dPositions1 = fnPositions1.create(positions1);
	hPositions1.set(dPositions1);
	st = data.setClean( aPositions1 );mser;
		 

	MDataHandle hUValues = data.outputValue(aUValues);
	MFnDoubleArrayData fnUValues;
	MObject dUValues = fnUValues.create(uValues);
	hUValues.set(dUValues);
	st = data.setClean( aUValues );mser;

	MDataHandle hMasses = data.outputValue(aMasses);
	MFnDoubleArrayData fnMasses;
	MObject dMasses = fnMasses.create(masses);
	hMasses.set(dMasses);
	st = data.setClean( aMasses );mser;


	MDataHandle hCount = data.outputValue( aCount );
	int& count = hCount.asInt();
	count = nPoints;
	hCount.setClean();
 

	return MS::kSuccess;
}


