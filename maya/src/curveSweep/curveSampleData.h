


#ifndef curveSampleData_H
#define curveSampleData_H

#include <maya/MPlug.h>
#include <maya/MPxNode.h>

class curveSampleData : public MPxNode
{
public:
	curveSampleData();
	virtual ~curveSampleData();
	virtual MStatus	compute( const MPlug&, MDataBlock& );
	
	static  void *	creator();
	static  MStatus	initialize();
	static	MTypeId		id;
	
	// enum Spacing { kParametric, kArcLen};

private:
		
	static	MObject	 aSweptCurve;
	static	MObject	 aClosedCurve;

	// outputs
	static	MObject  aPositions1;	
	static	MObject  aPositions0;	
	static	MObject  aUValues;
	static	MObject  aMasses;
	static	MObject  aCount;

	static	MObject  aDeltaTime;



};
#endif



