#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h"


#include <chain_locator.h>
#include <chain_mesh.h>
#include <chain_warp.h>
#include <chainArrayEmitter.h>
#include <chain_emitter.h>

#include <curve_chain_emitter.h>
#include <curve_chain_mesh.h>

#include <curveSampleData.h>

MStatus initializePlugin( MObject obj)
{

	MStatus st;
	
	MString method("initializePlugin");

	MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	st = plugin.registerNode(	"chainLocator",
		chainLocator::id,
		chainLocator::creator,
		chainLocator::initialize,
		MPxNode::kLocatorNode ); mser;

	st = plugin.registerNode( 
		"chainMesh",
		 chainMesh::id,
		 chainMesh::creator,
		 chainMesh::initialize );mser;

	st = plugin.registerNode( 
		"chainWarp",
		 chainWarp::id,
		 chainWarp::creator,
		 chainWarp::initialize );mser;


	st = plugin.registerNode( "chainEmitter", chainEmitter::id, chainEmitter::creator, chainEmitter::initialize, MPxNode::kEmitterNode  );mser;
	st = plugin.registerNode( "chainArrayEmitter", chainArrayEmitter::id, chainArrayEmitter::creator, chainArrayEmitter::initialize, MPxNode::kEmitterNode  );mser;


	st = plugin.registerNode( "curveChainEmitter", curveChainEmitter::id, curveChainEmitter::creator, curveChainEmitter::initialize, MPxNode::kEmitterNode  );mser;
	st = plugin.registerNode( "curveChainMesh", curveChainMesh::id, curveChainMesh::creator, curveChainMesh::initialize  );mser;
	st = plugin.registerNode( "curveSampleData", curveSampleData::id, curveSampleData::creator, curveSampleData::initialize);mser;
	


	MGlobal::executePythonCommand("import sleuth;sleuth.load()");

	return st;

}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;

	MString method("uninitializePlugin");

	MFnPlugin plugin( obj );

	st = plugin.deregisterNode( curveSampleData::id );mser;
	st = plugin.deregisterNode( curveChainMesh::id );mser;
	st = plugin.deregisterNode( curveChainEmitter::id );mser;

	st = plugin.deregisterNode( chainArrayEmitter::id );mser;

	st = plugin.deregisterNode( chainEmitter::id );mser;

	st = plugin.deregisterNode( chainWarp::id );mser;
	st = plugin.deregisterNode( chainMesh::id );
	st = plugin.deregisterNode( chainLocator::id );


	return st;
}


