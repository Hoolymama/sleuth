

#ifndef matrixChainData_H
#define matrixChainData_H
#include <map>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MObject.h>
#include <chain_data.h>


class matrixChainData : public chainData {
public:

	matrixChainData();

	matrixChainData (
		const MVectorArray &pos,
		const MDoubleArray &streamId,
		const MDoubleArray &streamParam,
		MStatus *st
		);
	~matrixChainData();

	MStatus assignPhis(const MVectorArray &arr);

	MStatus assignTwists(const MDoubleArray &arr);

	const MVector getPhi (const int nIndex) const ;

	const double getTwist (const int nIndex) const ;


	MStatus makeChains(double gap);
	MStatus makeChains(double gap,  MRampAttribute &rampAttr);
private:

	const MVectorArray * m_pPhi;

	const MDoubleArray * m_pTwist;

};

#endif


