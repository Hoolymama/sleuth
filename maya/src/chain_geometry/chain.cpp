/*
A single chain object

This is an array of pearls.

It has functions to calculate transformations 
along the chain, arc length, and to bind vertices.
*/


#include <math.h>
#include <algorithm>
#include <chain_data.h>
#include <errorMacros.h>
#include <maya/MMatrix.h>
#include <maya/MRampAttribute.h>

#include <mayaMath.h>
#include <pearl.h>

const double bigNum = 10e+31f;

chain::chain():
m_pearlArray()
{}

chain::~chain(){}


const PEARL_ARRAY & chain::pearls() const {return m_pearlArray;} ;

/*
get streamId and geometryId
*/
int chain::stream() const {
    int result = 0;
    if (m_pearlArray.size()) result = m_pearlArray[0]->streamId();
    return result;
}

int chain::geometryId() const {
    int result = 0;
    if (m_pearlArray.size()) result = m_pearlArray[0]->geometryId();
    return result;
}

int chain::length() const {return m_pearlArray.size();}

double chain::arcLength() const {
	if (m_pearlArray.size() < 2) return 0.0;
	double len = 0.0;
	PEARL_ARRAY::const_iterator curr_pearl = m_pearlArray.begin();
	PEARL_ARRAY::const_iterator next_pearl = curr_pearl +1;
	
	for (; next_pearl !=m_pearlArray.end(); ++curr_pearl,++next_pearl)
	{
		len += ((*curr_pearl)->position() - (*next_pearl)->position()).length();
	}
	return len;
}

/* for each pearl, store dist from previous pearl */
void chain::setDistances()  {
	if (m_pearlArray.size() < 2) return;
	float dist = 0.0f;
	PEARL_ARRAY::iterator prev_pearl = m_pearlArray.begin();
	PEARL_ARRAY::iterator curr_pearl = prev_pearl + 1;

	while (curr_pearl !=m_pearlArray.end())
	{
		dist += float((   (*curr_pearl)->position() - (*prev_pearl)->position()   ).length());
		(*curr_pearl)->setDistance(dist);
		curr_pearl++;
		prev_pearl++;
	}
}

// multiply radii by value of ramp att;
void chain::applyRadiusRamp(  MRampAttribute &rampAtt)  {
	PEARL_ARRAY::iterator curr_pearl = m_pearlArray.begin();
	int last = m_pearlArray.size() -1;
	float arclen = (m_pearlArray[last])->distance();
	if (arclen <= 0.0f) return;

	while (curr_pearl !=m_pearlArray.end())
	{
		float param =(*curr_pearl)->distance() / arclen;
		float radMult;
		rampAtt.getValueAtPosition(param, radMult ); 
		float rad = (*curr_pearl)->radius(); 
		(*curr_pearl)->setRadius((rad*radMult));
		curr_pearl++;
	}
}


// start and end matrix are at right angles to the segment direction
// while internal matricces are at angles between the two adjacent segs
void chain::setMatrices(const MVector &phi, double twist)  {

	if (m_pearlArray.size() < 2) return;
	PEARL_ARRAY::iterator prev_pearl, curr_pearl, next_pearl;
	curr_pearl = m_pearlArray.begin();
	next_pearl = curr_pearl + 1;


	MVector segDir = (*next_pearl)->position() -  (*curr_pearl)->position() ;
	
	MVector p = (*curr_pearl)->position();
	
	MMatrix mat = mayaMath::matFromPhi(phi);
	
	MVector  up = MVector(mat[1][0],mat[1][1],mat[1][2]);

	mat = mayaMath::matFromAim(p,segDir,up);
	
	(*curr_pearl)->setMatrix(mat);

	prev_pearl = curr_pearl;
	curr_pearl ++;
	next_pearl ++;

	 while (next_pearl != m_pearlArray.end()) 
	{
		MVector p = (*curr_pearl)->position();
	
		MVector lastSeg = p - (*prev_pearl)->position() ;
	
		MVector nextSeg =     (*next_pearl)->position() - p;
	
        MVector dir = lastSeg.rotateBy(MQuaternion(lastSeg,nextSeg,0.5)); //  the avgerage of segments
	
        up = MVector(mat[1][0],mat[1][1],mat[1][2]);
	
        MQuaternion qtwist(twist, dir);
	
        up = up.rotateBy(qtwist);
	
        mat = mayaMath::matFromAim(p,dir,up);
	
        (*curr_pearl)->setMatrix(mat);

        prev_pearl = curr_pearl;
        curr_pearl++;
        next_pearl++;
 	
   }
    p = (*curr_pearl)->position();
	
    segDir = p - (*prev_pearl)->position();
	
    up = MVector(mat[1][0],mat[1][1],mat[1][2]);
    MQuaternion qtwist(twist, segDir);
	
    up = up.rotateBy(qtwist);
	
   mat = mayaMath::matFromAim(p,segDir,up);
    (*curr_pearl)->setMatrix(mat);
	
}




int chain::binarySearch(float x) const {
	unsigned count = m_pearlArray.size();
	if (count < 2) return -1;

	unsigned low = 0;
	unsigned high = count-1;
	unsigned mid; 
	if (x < (m_pearlArray[low])->distance()) return -1;
	if (x >= (m_pearlArray[high])->distance()) return high;
	for (int i = 0; i < count; ++i)
	 {

		mid = (low+high)/2;
		if (x<(m_pearlArray[mid])->distance()) {
			if (x >= (m_pearlArray[mid-1])->distance()) return mid-1;
			high = mid;
		} else { // x is >= mid
			if (x < (m_pearlArray[mid+1])->distance()) return mid;
			low=mid;
		}
	}
	cerr << "Infinite loop problem in chain::binarySearch - Should never get here" << endl;
	return -1;
}


/*
The warp deformer needs to transform verts from an object by mapping
x position to a distance along the chain, and then transforming the vert
as if it had flowed down the chain to that point.

Therefore, in order to avoid hard angles in the result geometry, 
points must be deformed based on interpolated cordinate systems according to
their param along the curve. 
*/
MFloatPoint chain::interpolatedTransformation(const MFloatPoint &p, int i) const {

  int low = binarySearch(p.x);
  int last = m_pearlArray.size() -1;
  if (low == -1) {
    // before the start, so multily vert by first matrix

    float rad = (m_pearlArray[0])->radius();
    MFloatPoint resultPoint(p.x, p.y*rad,p.z*rad);
    return MFloatPoint(MPoint(resultPoint) * (m_pearlArray[0])->matrix());

  } else if (low >= last) {
    // after the end, multiply vert by last matrix
    float rad = (m_pearlArray[last])->radius();
    MFloatPoint resultPoint = MFloatPoint( 0.0 , (p.y*rad), (p.z*rad));
    return MFloatPoint( MPoint(resultPoint) * (m_pearlArray[last])->matrix());

  } else {

    // multiply vert by matrix interpolated over bounding segment
    float xOffset = p.x - (m_pearlArray[low])->distance();

    float param = xOffset / ((m_pearlArray[(low+1)])->distance() - (m_pearlArray[low])->distance()) ;
    float oneMinusParam = 1.0f - param;
    float rad = float((m_pearlArray[low])->radius() * oneMinusParam)  + ((m_pearlArray[low+1])->radius() * param);

    MFloatPoint resultPoint = MFloatPoint(0.0f, (p.y*rad), (p.z*rad));

    const MMatrix & m0 = (m_pearlArray[low])->matrix();
    const MMatrix & m1 = (m_pearlArray[(low+1)])->matrix();
    MQuaternion q0;q0 = m0;
    MQuaternion q1;q1 = m1;
    MQuaternion qres = slerp(q0,q1,double(param));

    MVector t0 = MVector(m0[3][0], m0[3][1],m0[3][2]);
    MVector t1 = MVector(m1[3][0], m1[3][1],m1[3][2]);
    MVector tres = (t0 * oneMinusParam) + (t1 * param);

    // construct new matrix
    MMatrix mres = qres.asMatrix();
    mres[3][0] = tres.x;
    mres[3][1] = tres.y;
    mres[3][2] = tres.z;
    // res transformed
    return MFloatPoint(MPoint(resultPoint) * mres);
  }
}


void chain::appendPearl(pearl * p) {m_pearlArray.push_back(p);}

