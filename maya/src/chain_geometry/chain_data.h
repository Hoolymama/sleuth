
#ifndef chainData_H
#define chainData_H

#include <maya/MIOStream.h>
#include <errorMacros.h>
#include <chain.h>

#include <pearl.h>

class chainData{
public:
	chainData();

	chainData (
		const MVectorArray &pos,
		const MDoubleArray &streamId,
		const MDoubleArray &streamParam,
		MStatus *st
		);

	MStatus assignGeometryIds(const MDoubleArray &arr);

	MStatus assignSnips(const MDoubleArray &arr);

	MStatus assignRadii(const MDoubleArray &arr) ;

	MStatus makeChains(double gap);

	~chainData();

protected:
	
	PEARL_ARRAY m_pearlArray;
	CHAIN_LIST m_chainList;

};

#endif

