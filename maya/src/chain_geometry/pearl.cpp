
#include <pearl.h>
#include <errorMacros.h>

pearl::pearl(){}

pearl::pearl(
        const MVector &pos, 
        double streamParam,
        int streamId,
        int metaId
):
m_position(pos),
m_streamParam(streamParam) ,
m_snip(0),
m_streamId(streamId),
m_metaId(metaId),
m_geometryId(-1),
m_distance(0.0f),
m_radius(1.0),
m_matrix()
{}

pearl::~pearl(){}

const MVector & pearl::position() const {return m_position; }  
const MMatrix & pearl::matrix() const {return m_matrix; }  
const double  & pearl::streamParam() const {return m_streamParam;  }  
const float   & pearl::distance() const {return m_distance;  }  
const int     & pearl::streamId() const {return m_streamId; }    
const int     & pearl::snip() const {return m_snip; }    
const int     & pearl::metaId() const {return m_metaId; }    
const int     & pearl::geometryId() const {return m_geometryId; }
   
const double  & pearl::radius() const {return m_radius;  }               


void pearl::setPosition(const MVector  & v) {m_position = v;}
void pearl::setMatrix(const MMatrix  & m) {m_matrix = m;}
void pearl::setStreamParam(const double  & v)     {m_streamParam = v;}      
void pearl::setDistance(const float  & v)     {m_distance = v;}      
void pearl::setSnip(int v)                 {m_snip = v;}      
void pearl::setStreamId(int v)              {m_streamId = v;}        
void pearl::setMetaId(int v)              {m_metaId = v;}        
void pearl::setGeometryId(int v)             {m_geometryId = v;}          
void pearl::setRadius(const double & v)     {m_radius = v;}    
