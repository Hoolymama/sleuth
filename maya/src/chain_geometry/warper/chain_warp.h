


#include <maya/MPointArray.h>
#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MStatus.h>

class chainWarp : public MPxNode
{
public:
	chainWarp();
	virtual           ~chainWarp();

	virtual MStatus   compute( const MPlug&, MDataBlock& );
	virtual void      postConstructor();

	static  void *    creator();
	static  MStatus   initialize();
	static  MTypeId   id;



	MObject calcDefaultMesh( int numSegments, MVector scale) const ;

private:

	static MObject aPosition;
	static MObject aPhi;
	static MObject aStreamParam;
	static MObject aStreamIdPP;
	static MObject aRadiusPP;
	static MObject aTwistPP;
	static MObject aBreakPP;
	static MObject aGapThreshold;

	static MObject aMeshIdPP;
	static MObject aDefaultMeshSegments;
	static MObject aDefaultMeshScaleX;
	static MObject aDefaultMeshScaleY;
	static MObject aDefaultMeshScaleZ;
	static MObject aDefaultMeshScale;
	static MObject aFlowMeshes;
	static MObject aRadiusRamp;
	static MObject aMeshSet;


	static MObject aOutMesh;


};
