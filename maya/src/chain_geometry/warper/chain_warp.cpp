

#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MPxNode.h>
#include <maya/MStatus.h>

#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPluginData.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MRampAttribute.h>

#include <maya/MFnNurbsCurve.h>
#include <maya/MFnMeshData.h>
#include <maya/MFloatPoint.h>
#include <maya/MFnMesh.h>
#include <maya/MTime.h>

#include <mayaMath.h>
#include "errorMacros.h"
#include "jMayaIds.h"

#include <warp_chain_data.h>
#include "chain_warp.h"



MObject chainWarp::aPosition;
MObject chainWarp::aPhi;
MObject chainWarp::aStreamParam;
MObject chainWarp::aStreamIdPP;
MObject chainWarp::aRadiusPP;
MObject chainWarp::aTwistPP;
MObject chainWarp::aBreakPP;
MObject chainWarp::aGapThreshold;

MObject chainWarp::aMeshIdPP;
MObject chainWarp::aDefaultMeshSegments;
MObject chainWarp::aDefaultMeshScaleX;
MObject chainWarp::aDefaultMeshScaleY;
MObject chainWarp::aDefaultMeshScaleZ;
MObject chainWarp::aDefaultMeshScale;
MObject chainWarp::aRadiusRamp;

MObject chainWarp::aFlowMeshes;
MObject chainWarp::aMeshSet;

MObject chainWarp::aOutMesh;




const double PI_X2 = 6.283185306;
const double PI = 3.141592653;

MTypeId chainWarp::id( k_chainWarp );

chainWarp::chainWarp() {}

chainWarp::~chainWarp()
{
}

void* chainWarp::creator()
{
    return new chainWarp();
}
void chainWarp::postConstructor( ){}


MStatus chainWarp::initialize()
{
    MStatus st;

    MString method("chainWarp::initialize");
    MFnNumericAttribute nAttr; 
    MFnTypedAttribute tAttr; 
    MFnCompoundAttribute cAttr; 
    MFnUnitAttribute uAttr;

    aPosition = tAttr.create("position", "pos", MFnData::kVectorArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    tAttr.setCached(false);
    st = addAttribute(aPosition);mser;

    aPhi = tAttr.create("phi", "phi", MFnData::kVectorArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    tAttr.setCached(false);
    st = addAttribute(aPhi);mser;

    aStreamParam = tAttr.create("streamParam", "spm", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aStreamParam);mser;

    aStreamIdPP = tAttr.create("streamId", "sid", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aStreamIdPP);mser;

    aRadiusRamp = MRampAttribute::createCurveRamp("radiusRamp","rrmp");
    st = addAttribute( aRadiusRamp );mser;
    // aRadiusRamp.setValueAtIndex (1.0f,0, &st);mser;


    aRadiusPP = tAttr.create("radius", "rad", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aRadiusPP);mser;

    aTwistPP = tAttr.create("twist", "tws", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aTwistPP);mser;

    aBreakPP = tAttr.create("break", "brk", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aBreakPP);mser;


    aGapThreshold = nAttr.create( "gapThreshold", "gt",MFnNumericData::kDouble );
    nAttr.setStorable(true);
    st =  addAttribute(aGapThreshold);  mser;


    aMeshIdPP = tAttr.create("meshId", "msid", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aMeshIdPP);mser;

    aDefaultMeshSegments = nAttr.create("defaultMeshSegments","dmss", MFnNumericData::kLong);
    nAttr.setKeyable(true);
    nAttr.setStorable(true);
    nAttr.setDefault(3);
    addAttribute( aDefaultMeshSegments );
    
    aDefaultMeshScaleX = nAttr.create( "defaultMeshScaleX", "dmsx", MFnNumericData::kDouble , 1.0  );
    aDefaultMeshScaleY = nAttr.create( "defaultMeshScaleY", "dmsy", MFnNumericData::kDouble , 1.0  );
    aDefaultMeshScaleZ = nAttr.create( "defaultMeshScaleZ", "dmsz", MFnNumericData::kDouble , 1.0  );
    aDefaultMeshScale = nAttr.create(  "defaultMeshScale",  "dms", aDefaultMeshScaleX, aDefaultMeshScaleY, aDefaultMeshScaleZ );
    nAttr.setWritable(true);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    addAttribute(aDefaultMeshScale);

    aMeshSet = tAttr.create("meshSet","mst", MFnData::kMesh );
    tAttr.setStorable(false);
    tAttr.setReadable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    tAttr.setArray(true);
    tAttr.setIndexMatters(false);

    aFlowMeshes = cAttr.create("flowMeshes","fms");
    cAttr.setArray(true);
    cAttr.addChild(aMeshSet);
    cAttr.setArray(true);
    cAttr.setIndexMatters(true);
    cAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aFlowMeshes);


    aOutMesh = tAttr.create( "outMesh", "om", MFnData::kMesh );
    tAttr.setStorable(false);
    tAttr.setWritable(false);
    addAttribute( aOutMesh );



    // attributeAffects (aVCoordLength, aOutMesh);
    attributeAffects (aPosition, aOutMesh);
    attributeAffects (aPhi, aOutMesh);
    attributeAffects (aRadiusPP, aOutMesh);
    attributeAffects (aStreamParam, aOutMesh);
    attributeAffects (aStreamIdPP, aOutMesh);
    attributeAffects (aGapThreshold, aOutMesh);
    attributeAffects (aBreakPP, aOutMesh);
    attributeAffects (aTwistPP, aOutMesh);
    attributeAffects (aRadiusRamp, aOutMesh);

    attributeAffects (aMeshIdPP, aOutMesh);
    attributeAffects (aDefaultMeshSegments, aOutMesh);
    attributeAffects (aDefaultMeshScaleX, aOutMesh);
    attributeAffects (aDefaultMeshScaleY, aOutMesh);
    attributeAffects (aDefaultMeshScaleZ, aOutMesh);
    attributeAffects (aDefaultMeshScale, aOutMesh);
    attributeAffects (aFlowMeshes, aOutMesh);
    attributeAffects (aMeshSet, aOutMesh);


    return MS::kSuccess;
}


MObject chainWarp::calcDefaultMesh( int numSegments, MVector scale) const 
{
 //   cerr << "chainWarp::calcDefaultMesh" << endl;

    MStatus st;
    //MFnMeshData fnC;
    //MObject outGeom = fnC.create(&st);mser;
    //MFnMesh fnM;
    //MObject geom;
    MFloatPointArray vertices ;    
    MIntArray faceCounts, connectivity, uvCounts, uvIds;
    MFloatArray uVals, vVals;


    if (numSegments < 1) numSegments = 1;

    // verts
   /////////////////////////////////////
    float fNumSegments = float(numSegments);
    for (int i = 0; i <= numSegments; ++i)
    {
        float x = (i/fNumSegments) * scale.x;
        float y = 0.5 * scale.y;
        float z = 0.5 * scale.z;
        vertices.append(MFloatPoint(x,-y,-z));
        vertices.append(MFloatPoint(x,y,-z));
        vertices.append(MFloatPoint(x,y,z));
        vertices.append(MFloatPoint(x,-y,z));   
    }
    // counts and connects
    /////////////////////////////////////
    int vertexId = 0;
    faceCounts.append(4);
    for (unsigned k = 0;k<4;k++) {
        int the_id =  vertexId + (3 - k);
        connectivity.append(the_id);
    }

    for (unsigned j = 0;j<(numSegments);j++) {
     
        for (unsigned k = 0;k<3;k++) {
            faceCounts.append(4);
            connectivity.append(vertexId);
            connectivity.append(vertexId+1);
            connectivity.append(vertexId+5);
            connectivity.append(vertexId+4);

            vertexId+=1;
        }
        // close the tube
        faceCounts.append(4);
        connectivity.append(vertexId);
        connectivity.append(vertexId-3);
        connectivity.append(vertexId+1);
        connectivity.append(vertexId+4);
        vertexId +=1;
    }
    faceCounts.append(4);
    for (unsigned k = 0;k<4;k++) {
        connectivity.append(vertexId + k);
    }

    // UVs
    /////////////////////////////////////
    // first end
    uVals.append(0.0f);uVals.append(0.0f);uVals.append(1.0f);uVals.append(1.0f);
    vVals.append(0.0f);vVals.append(-1.0f);vVals.append(-1.0f);vVals.append(0.0f);
    // middle
    for (int i = 0; i <= numSegments; ++i)
    {
        float v = (i/fNumSegments);
        uVals.append(0.0f);vVals.append(v);
        uVals.append(0.25f);vVals.append(v);
        uVals.append(0.5f);vVals.append(v);
        uVals.append(0.75f);vVals.append(v);
        uVals.append(1.0f);vVals.append(v);
    }
    // last end
    uVals.append(0.0f);uVals.append(0.0f);uVals.append(1.0f);uVals.append(1.0f);
    vVals.append(1.0f);vVals.append(2.0f);vVals.append(2.0f);vVals.append(1.0f);


    // uv mapping
   // start cap
    int uvIndex = 0;
    uvCounts.append(4);
    for (unsigned k = 0;k<4;k++) {
        int the_id =  uvIndex + (3 - k);
        uvIds.append(the_id) ;
    }
    uvIndex +=4;

    for (unsigned j = 0;j<(fNumSegments);j++) {
        
        for (unsigned k = 0;k<4;k++) {
            uvCounts.append(4);
            uvIds.append(uvIndex);
            uvIds.append(uvIndex+1);
            uvIds.append(uvIndex+6);
            uvIds.append(uvIndex+5);

            uvIndex+=1; // work our way round.
        }
        uvIndex+=1; // jump up one step.
    }
    uvIndex+=5; // jump off the end

    // end cap
    uvCounts.append(4);
    for (unsigned k = 0;k<4;k++) {
        uvIds.append(uvIndex);
        uvIndex ++;
    }

    MFnMesh fnMesh;
    MFnMeshData dataCreator;
    MObject data = dataCreator.create();
    // JPMDBG;
    // cerr << "vertices.length() "<< vertices.length() << endl;
    // cerr << "faceCounts.length() "<< faceCounts.length() << endl;
    


    fnMesh.create( 
        vertices.length(), 
        faceCounts.length(), 
        vertices, 
        faceCounts, 
        connectivity, data, &st 
    );  mser;



    st = fnMesh.setUVs(uVals, vVals);  mser; 
    st = fnMesh.assignUVs ( uvCounts, uvIds ); mser; 

    return data;
}



MStatus chainWarp::compute( const MPlug& plug,  MDataBlock& data ) 
{ 
    MStatus st;

    MString method("chainWarp::compute");


    if(plug != aOutMesh) return MS::kUnknownParameter;


    MFnMeshData fnC;
    MObject outGeom = fnC.create(&st);mser;
    MFnMesh fnM;

    MFnVectorArrayData fnV;
    MFnDoubleArrayData fnD;
    MDataHandle h;
    MObject d;

    unsigned pl = 0;

    h = data.inputValue(aPosition, &st);msert;
    d = h.data();
    st = fnV.setObject(d);msert;
    pl = fnV.length();
    const MVectorArray & pos = fnV.array(); 


    // clean the mesh if no points
    if ( !pl) {
        fnM.create( 0, 0, MFloatPointArray(), MIntArray(), MIntArray(), outGeom, &st );  mser;
        MDataHandle hMesh = data.outputValue(aOutMesh, &st);
        hMesh.set(outGeom);
        data.setClean(aOutMesh);
        return MS::kUnknownParameter;   
    }

    
    h = data.inputValue(aStreamIdPP, &st);msert;
    d = h.data();
    st = fnD.setObject(d);msert;
    if (fnD.length() != pl) {st=MS::kUnknownParameter;msert;}
    const MDoubleArray & sid = fnD.array(); 


    h = data.inputValue(aStreamParam, &st);msert;
    d = h.data();
    st = fnD.setObject(d);msert;
    if (fnD.length()  != pl) {st=MS::kUnknownParameter;msert;}
    const MDoubleArray & streamParam = fnD.array(); 



    MVectorArray phi ;
    h = data.inputValue(aPhi, &st);
    if (! st.error()) {
        d = h.data();
        st = fnV.setObject(d);
        if (! st.error()) {
            if (fnV.length() == pl) {
                phi = fnV.array();  
            }   
        }
    }

    MDoubleArray rad ;
    h = data.inputValue(aRadiusPP, &st);
    if (! st.error()) {
        d = h.data();
        st = fnD.setObject(d);
        if (! st.error()) {
            if (fnD.length() == pl) {
                rad = fnD.array();  
            }   
        }
    }
    
    MDoubleArray twist ;
    h = data.inputValue(aTwistPP, &st);
    if (! st.error()) {
        d = h.data();
        st = fnD.setObject(d);
        if (! st.error()) {
            if (fnD.length() == pl) {
                twist = fnD.array();    
            }   
        }
    }

    MDoubleArray snip ;
    h = data.inputValue(aBreakPP, &st);
    if (! st.error()) {
        d = h.data();
        st = fnD.setObject(d);
        if (! st.error()) {
            if (fnD.length() == pl) {
                snip = fnD.array(); 
            }   
        }
    }


    MDoubleArray msid ;
    h = data.inputValue(aMeshIdPP, &st);
    if (! st.error()) {
        d = h.data();
        st = fnD.setObject(d);
        if (! st.error()) {
            if (fnD.length() == pl) {
                msid = fnD.array(); 
            }   
        }
    }


    double gt =data.inputValue( aGapThreshold).asDouble();
    if (gt == 0.0) gt = 99999999.0;


    int numDefaultSegs = data.inputValue(aDefaultMeshSegments).asShort();
    if (numDefaultSegs < 1) numDefaultSegs = 1;
    MVector defaultMeshScale = data.inputValue(aDefaultMeshScale).asVector();

    // cerr << "made a default mesh" << endl;
    // MFnMesh defaultMeshFn(defaultMesh);
    // JPMDBG;
    // cerr << "defaultMeshFn.numVertices() "<< defaultMeshFn.numVertices() << endl;

    // make a map of MPointArrays.
    warpChainData chains(pos,sid,streamParam,&st);msert;

    if (phi.length() == pl) chains.assignPhis(phi);
    if (rad.length() == pl) chains.assignRadii(rad);
    if (twist.length() == pl) chains.assignTwists(twist);
    if (snip.length() == pl) chains.assignSnips(snip);
    if (msid.length() == pl) chains.assignGeometryIds(msid);


    MObject defaultMesh =  calcDefaultMesh(numDefaultSegs, defaultMeshScale) ;

    chains.addSourceMesh(-1,defaultMesh);

    MArrayDataHandle hac = data.inputArrayValue(aFlowMeshes);
    unsigned nf = hac.elementCount();
    MObject dc;
    for(unsigned i = 0;i < nf; i++, hac.next()) {
        int id = hac.elementIndex(&st);
        MDataHandle hccompound = hac.inputValue();
        MArrayDataHandle haset = hccompound.child(aMeshSet);
        unsigned nsets = haset.elementCount();
        for(unsigned j = 0;j < nsets; j++, haset.next()) {

            MDataHandle hc = haset.inputValue();
            //MPointArray pts;
            dc = hc.asMeshTransformed();
            //MFnNurbsCurve fnCurve( dc, &st ); mser;
            //fnCurve.getCVs(pts);
            //unsigned plen = pts.length();
            //if (plen > 1) {
            chains.addSourceMesh(id,dc);
            //}
        }
    }
    

    
    MRampAttribute rampAttr( thisMObject(), aRadiusRamp, &st ); mser;

    chains.makeChains(gt, rampAttr);
  
    chains.deform(outGeom);
 
    MDataHandle hMesh = data.outputValue(aOutMesh, &st);
    hMesh.set(outGeom);
    data.setClean(aOutMesh);

    return MS::kSuccess;
}