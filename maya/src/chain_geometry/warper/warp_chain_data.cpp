/*
warp_chain_data is chain_data with the ability to generate and deform some geometry along the chains.
*/
#include <warp_chain_data.h>

#include <maya/MFnMesh.h>
warpChainData::warpChainData(){}

warpChainData::warpChainData (
     const MVectorArray &pos,
     const MDoubleArray &streamId,
     const MDoubleArray &streamParam,
     MStatus *st
) 
:matrixChainData(pos,streamId,streamParam,st),
m_vertices(), 
m_faceCounts(), 
m_connectivity(),
m_uVals(), 
m_vVals(),
m_uvCounts(), 
m_uvIds(),
m_vertexIndex(0),
m_uvIndex(0),
m_uvOffset(0)
{}

warpChainData::~warpChainData(){
 
    MESH_GROUP_MAP::iterator mapiter = m_meshGroupMap.begin();
    while (mapiter != m_meshGroupMap.end()) {
        MESH_GROUP::iterator giter = mapiter->second.begin();
        while (giter != mapiter->second.end()) {
            delete *giter;
            *giter = 0;
            giter++;
        }
        mapiter++;
    }
}

// register a mesh
void warpChainData::addSourceMesh(int id, const MObject &geo){
    MStatus st;

    MESH_GROUP_MAP::iterator mapiter = m_meshGroupMap.find(id);
    if (mapiter == m_meshGroupMap.end()) {
        MESH_GROUP meshGroup;
        m_meshGroupMap[id] = meshGroup;
        mapiter = m_meshGroupMap.find(id);
    }
    meshData * md = new meshData(geo,m_uvOffset);
    mapiter->second.push_back(md);

    const MFloatArray & u = md->uVals();
    const MFloatArray & v = md->vVals();
    int numUVs = u.length();
    appendUVs(u,v);

    // increment m_uvOffset after setting it for the meshData object
    // so that the next meshData will havve the correct offset into the UV arrays 
    m_uvOffset += numUVs;
}

// 
void warpChainData::appendUVs(const MFloatArray & uVals,const MFloatArray & vVals){
    unsigned len = uVals.length();
    for (int i = 0; i < len; ++i) {
        m_uVals.append(uVals[i]);
        m_vVals.append(vVals[i]);
    }
}


const MESH_GROUP & warpChainData::getMeshGroup(int id)  const {
    MESH_GROUP_MAP::const_iterator mapiter = m_meshGroupMap.find(id);
    if (mapiter != m_meshGroupMap.end()) {
        return mapiter->second; 
    } else {
        mapiter = m_meshGroupMap.find(-1);
        return mapiter->second; 
   }
}


void warpChainData::deform(MObject &geom) 
{
    MStatus st;

    CHAIN_LIST::const_iterator itCurrChain = m_chainList.begin();
    int vertexId = 0;
    while (itCurrChain != m_chainList.end()) {
       const chain & cur_chain = **itCurrChain;

       const MESH_GROUP & meshes = getMeshGroup((*itCurrChain)->geometryId());
       MESH_GROUP::const_iterator itMesh = meshes.begin();
       while (itMesh != meshes.end()) {
            deformVertices(cur_chain,**itMesh);
            itMesh++;
       }
   
       itCurrChain ++;
   }

   MFnMesh fnMesh;

   fnMesh.create( 
       m_vertices.length(), 
       m_faceCounts.length(), 
       m_vertices, 
       m_faceCounts, 
       m_connectivity, geom, &st 
       );  mser;

 
    
   st = fnMesh.setUVs(m_uVals, m_vVals);  mser; 
   st = fnMesh.assignUVs ( m_uvCounts, m_uvIds ); mser; 


}


void warpChainData::appendPoints(const MFloatPointArray &curr_verts)  {
     unsigned len = curr_verts.length();
    for (int i = 0; i < len; ++i) {m_vertices.append(curr_verts[i]);}
}

void warpChainData::appendCounts(const MIntArray &counts) {
    int n = counts.length();
    for (int i = 0; i < n; ++i)
    {
        m_faceCounts.append(counts[i]);
    }
}

void warpChainData::appendUvCounts(const MIntArray &uvCounts){
 int n = uvCounts.length();
    for (int i = 0; i < n; ++i)
    {
        m_uvCounts.append(uvCounts[i]);
    }
}

void warpChainData::appendUvIds(const MIntArray &uvIds, int offset){
    int n = uvIds.length();
    for (int i = 0; i < n; ++i)
    {
        m_uvIds.append(uvIds[i]+offset);
    }
}


void warpChainData::appendConnects(const MIntArray &connectivity, int offset) {

    int n = connectivity.length();
    for (int i = 0; i < n; ++i)
    {
        m_connectivity.append(connectivity[i]+offset);
    }
}

 void warpChainData::deformVertices(const chain &sweepChain, const meshData &mesh)  
 {
 
    MStatus st;

  
    const MFloatPointArray & verts = mesh.vertices();

    const MIntArray & counts  = mesh.faceCounts();
    const MIntArray & connects  = mesh.connectivity();

    unsigned nVerts = verts.length();   
    MFloatPointArray deformedVerts(nVerts);

    int last = sweepChain.length() -1;
    // for every vert, find its place in the chain.
    // cerr << "SKINNING" << endl;
    //  cerr << "******************************************************" << endl;
    for (int i = 0; i < nVerts; ++i)
    {
        MFloatPoint & dv = deformedVerts[i];
        dv = sweepChain.interpolatedTransformation(verts[i], i);
    }
    // cerr << "******************************************************" << endl;
    
    // must get value before appendPoints()
    int connectivityOffset = m_vertices.length();
    appendPoints(deformedVerts);
    appendConnects(connects, connectivityOffset);
    appendCounts(counts);

    MIntArray   m_uvCounts;
    MIntArray   m_uvIds;
    const MIntArray & uvCounts  = mesh.uvCounts();
    const MIntArray & uvIds  = mesh.uvIds();
    int uvOffset = mesh.uvOffset();
    appendUvCounts(uvCounts);

    appendUvIds(uvIds, uvOffset);


 }





