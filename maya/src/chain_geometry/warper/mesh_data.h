

#ifndef meshData_H
#define meshData_H

#include <maya/MIOStream.h>
#include <vector>
#include <map>

#include <maya/MFloatPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>

#include <errorMacros.h>
// #include <mayaMath.h>

class meshData{

public:

	meshData();
	meshData(const MObject &geo, int uvOffset);


	~meshData();

	const MFloatPointArray & vertices() const;
	const MIntArray & faceCounts() const;
	const MIntArray & connectivity() const;
	const MIntArray & uvCounts() const;
	const MIntArray & uvIds() const;
	const MFloatArray & uVals() const;
	const MFloatArray & vVals() const;
	int uvOffset() const;
private:
		
	MFloatPointArray m_vertices ;	
	MIntArray   m_faceCounts;
	MIntArray   m_connectivity;
	MIntArray   m_uvCounts;
	MIntArray   m_uvIds;
	MFloatArray m_uVals;
	MFloatArray m_vVals;
	int m_uvOffset;

};


typedef std::vector<meshData*> MESH_GROUP;

typedef std::map<int, MESH_GROUP>  MESH_GROUP_MAP;

#endif