
#ifndef warpChainData_H
#define warpChainData_H
// #include <map>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MObjectArray.h>

#include <maya/MObject.h>
#include <matrix_chain_data.h>
#include <mesh_data.h>

// typedef std::map<int, MObjectArray>  MESH_GROUP_MAP;

class warpChainData : public matrixChainData {
public:
	warpChainData();
	warpChainData (
		const MVectorArray &pos,
		const MDoubleArray &streamId,
		const MDoubleArray &streamParam,
		MStatus *st
		);
	~warpChainData();

	void addSourceMesh(int id, const MObject &geo);

	void deform(MObject &geom) ;


	void deformVertices(const chain &sweepChain, const meshData &mesh) ;
	void appendPoints(const MFloatPointArray &curr_verts)  ;

	void appendCounts(const MIntArray &counts) ;

	void appendConnects(const MIntArray &connectivity, int offset) ;

	void appendUVs(const MFloatArray & uVals,const MFloatArray & vVals);

	void appendUvCounts(const MIntArray &uvCounts);

	void appendUvIds(const MIntArray &uvIds, int offset);
private:


	const MESH_GROUP & getMeshGroup(int id)  const ;

	MESH_GROUP_MAP m_meshGroupMap;


	MFloatPointArray m_vertices ;	
	MIntArray m_faceCounts;
	MIntArray m_connectivity;
	MIntArray m_uvCounts;
	MIntArray m_uvIds;
	MFloatArray m_uVals;
	MFloatArray m_vVals;
	int m_vertexIndex;
	int m_uvIndex;

	int m_uvOffset;
};

#endif


