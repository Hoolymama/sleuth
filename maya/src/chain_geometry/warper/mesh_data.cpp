
#include <maya/MFnMesh.h>

#include <errorMacros.h>
#include <mesh_data.h>

meshData::meshData(){}

meshData::meshData(const MObject &geo, int uvOffset)
:m_uvOffset(uvOffset)
{
	MStatus st;
 
	MFnMesh meshFn(geo);
	st = meshFn.getPoints(m_vertices);mser;
	st = meshFn.getVertices(m_faceCounts,m_connectivity);mser;
	st = meshFn.getUVs( m_uVals,  m_vVals) ;mser;
	st = meshFn.getAssignedUVs( m_uvCounts, m_uvIds) ; mser;
}


const MFloatPointArray &  meshData::vertices() const { return m_vertices;};	
const MIntArray &    meshData::faceCounts() const {   return m_faceCounts;}
const MIntArray &    meshData::connectivity() const {   return m_connectivity;}
const MIntArray &    meshData::uvCounts() const {   return m_uvCounts;}
const MIntArray &    meshData::uvIds() const {   return m_uvIds;}
const MFloatArray &  meshData::uVals() const { return m_uVals;}
const MFloatArray &  meshData::vVals() const { return m_vVals;}
int meshData::uvOffset() const {return m_uvOffset;} 
	//int  meshData::uvOffset() const { return m_uvOffset;}


meshData::~meshData(){}