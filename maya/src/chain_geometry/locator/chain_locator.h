/*
Simple maya locator node that uses gl_chain_data to draw lines in the viewport.
*/

#ifndef _CHAIN_LOCATOR_H
#define _CHAIN_LOCATOR_H


 #if defined(linux)
#ifndef LINUX
 #define LINUX
#endif
 #endif

#if defined(OSMac_MachO_)
#include <AGL/agl.h>
#include <GLUT/glut.h>
#elif defined(WIN32)
#include <GL/gl.h>
#include <GL/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <maya/MIOStream.h>
#include <maya/MTime.h>

#include <maya/MFloatVectorArray.h>
#include <maya/MPxLocatorNode.h>
// #include <maya/MStringArray.h>
class chainLocator : public MPxLocatorNode
{
public:
        chainLocator();
        virtual ~chainLocator();

    virtual MStatus             compute( const MPlug& plug, MDataBlock& data );

        virtual void            draw( M3dView & view, const MDagPath & path,
                                                                  M3dView::DisplayStyle style,
                                                                  M3dView::DisplayStatus status );

        virtual bool            isBounded() const;

        static  void *          creator();
        static  MStatus         initialize();

        static  MTypeId         id;
        
private:
        
        static  MObject         aPosition;
        static  MObject         aColorPP;
        static  MObject         aStreamParam;
        static  MObject         aStreamIdPP;
        static  MObject         aBreakPP;
        static  MObject         aGapThreshold; // time
        static  MObject         aOutDummy; 

        MFloatVectorArray m_start;
        MFloatVectorArray m_end;
        MFloatVectorArray m_col_start;
        MFloatVectorArray m_col_end;


};
#endif