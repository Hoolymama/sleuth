
/*
We add the ability to associate per vertex colors with the chains. Then there is a function to give back arrays with which to build openGL lines.
*/

#include <gl_chain_data.h>


#include <maya/MFnMesh.h>
glChainData::glChainData(){}

glChainData::glChainData (
     const MVectorArray &pos,
     const MDoubleArray &streamId,
     const MDoubleArray &streamParam,
     MStatus *st
) 
:chainData(pos,streamId,streamParam,st),
m_pColor(0)
{}


glChainData::~glChainData(){}


MStatus glChainData::assignColors(const MVectorArray &arr){
    unsigned pl = m_pearlArray.size();
    if (arr.length() != pl) return MS::kUnknownParameter;
    m_pColor = &arr;
    return MS::kSuccess;
}

const MVector glChainData::getColor (const int nIndex) const 
{
    if (m_pColor && (nIndex < m_pColor->length()))
        return (*m_pColor)[nIndex];
    return MVector::xAxis; // red
}

/* generate data for GL lines with associated per vertex colors */
void glChainData::glLines(
    MFloatVectorArray & start,
    MFloatVectorArray & end,
    MFloatVectorArray & col_start,
    MFloatVectorArray & col_end
) const {
    MStatus st;
    start.clear();
    end.clear();
    col_start.clear();
    col_end.clear();

    CHAIN_LIST::const_iterator itCurrChain = m_chainList.begin();
    while (itCurrChain != m_chainList.end()) {
        const chain & cur_chain = **itCurrChain;

        PEARL_ARRAY::const_iterator curr_pearl = cur_chain.pearls().begin();
        PEARL_ARRAY::const_iterator next_pearl = curr_pearl + 1;
        while (next_pearl != cur_chain.pearls().end()) {
            int curr_metaId = (*curr_pearl)->metaId();
            int next_metaId = (*next_pearl)->metaId();
            start.append(MFloatVector((*curr_pearl)->position()));
            end.append(MFloatVector((*next_pearl)->position()));
            col_start.append(MFloatVector(getColor(curr_metaId)));
            col_end.append(MFloatVector(getColor(next_metaId)));
            curr_pearl++;
            next_pearl++;
        }
        itCurrChain++;
    }
}