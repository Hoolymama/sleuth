#include <maya/MVectorArray.h>
#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MAnimControl.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatVector.h>
#include <errorMacros.h>

// #include <string>
#include <sstream>
#include <gl_chain_data.h>
#include <chain_locator.h>
#include <maya/M3dView.h>
#include "jMayaIds.h"


MObject chainLocator::aPosition;
MObject chainLocator::aColorPP;
MObject chainLocator::aStreamParam;
MObject chainLocator::aStreamIdPP;
MObject chainLocator::aBreakPP;
MObject chainLocator::aGapThreshold; // time
MObject chainLocator::aOutDummy; 

MTypeId chainLocator::id( k_chainLocator );

chainLocator::chainLocator() {}
chainLocator::~chainLocator() {}

MStatus chainLocator::compute( const MPlug& plug, MDataBlock& data )
{ 

    MStatus st;
    MString method("chainLocator::compute");
    if (plug != aOutDummy)  return MS::kUnknownParameter;

        // JPMDBG;


    MFnVectorArrayData fnV;
    MFnDoubleArrayData fnD;
    MDataHandle h;
    MObject d;
    unsigned pl = 0;
        // JPMDBG;

    h = data.inputValue(aPosition, &st);msert;
    d = h.data();
    st = fnV.setObject(d);msert;
    pl = fnV.length();
    if ( pl == 0) {
        m_start.clear();
        m_end.clear();
        m_col_start.clear();
        m_col_end.clear();
        return MS::kSuccess;
    }
    const MVectorArray & pos = fnV.array(); 




    h = data.inputValue(aStreamIdPP, &st);msert;
    d = h.data();
    st = fnD.setObject(d);msert;
    if (fnD.length() < 1) {st=MS::kUnknownParameter;msert;}
    const MDoubleArray & streamId = fnD.array(); 
        // JPMDBG;

    h = data.inputValue(aStreamParam, &st);msert;
    d = h.data();
    st = fnD.setObject(d);msert;
    if (fnD.length() < 1) {st=MS::kUnknownParameter;msert;}
    const MDoubleArray & streamParam = fnD.array();       
        // JPMDBG;

    MDoubleArray snip ;
    h = data.inputValue(aBreakPP, &st);
    if (! st.error()) {
        d = h.data();
        st = fnD.setObject(d);
        if (! st.error()) {
            if (fnD.length() == pl) {
                snip = fnD.array(); 
            }   
        }
    }

    MVectorArray col ;
    h = data.inputValue(aColorPP, &st);
    if (! st.error()) {
        d = h.data();
        st = fnV.setObject(d);
        if (! st.error()) {
            if (fnV.length() == pl) {
                col = fnV.array();  
            }   
        }
    }



    double gt =data.inputValue( aGapThreshold).asDouble();
    if (gt == 0.0) gt = 99999999.0;

        // JPMDBG;

    glChainData chains(pos,streamId,streamParam,&st);msert;
    if (col.length() == pl) chains.assignColors(col);
    if (snip.length() == pl) chains.assignSnips(snip);

    chains.makeChains(gt);
    chains.glLines(m_start, m_end, m_col_start, m_col_end);


    return MS::kSuccess;
}

void chainLocator::draw( M3dView & view, const MDagPath & path, 
    M3dView::DisplayStyle style,
    M3dView::DisplayStatus status )
{ 

    MStatus st;


    int dummyValue;
    MObject thisNode = thisMObject();
    MPlug dummyPlug( thisNode, aOutDummy );
    dummyPlug.getValue( dummyValue );       



    view.beginGL(); 


    if ( status == M3dView::kActive ) {
        view.setDrawColor( 13, M3dView::kActiveColors );
    } else {
        view.setDrawColor( 12, M3dView::kActiveColors );
    }  


        // unsigned i;
    unsigned pl = m_start.length();


    glPushAttrib(GL_CURRENT_BIT);
    glBegin( GL_LINES );            
    for ( unsigned i = 0; i < pl; i++ ) {   
        glColor3f(m_col_start[i].x , m_col_start[i].y , m_col_start[i].z );
        glVertex3f( m_start[i].x , m_start[i].y , m_start[i].z );
        glColor3f(m_col_end[i].x , m_col_end[i].y , m_col_end[i].z );
        glVertex3f(     m_end[i].x , m_end[i].y, m_end[i].z);
    } 
    glEnd();                        
    glPopAttrib();


    view.endGL();
}

bool chainLocator::isBounded() const
{ 
    return false;
}

void* chainLocator::creator()
{
    return new chainLocator();
}

MStatus chainLocator::initialize()
{ 
    MStatus st;
    MFnNumericAttribute nAttr;
    MFnTypedAttribute tAttr;
    MFnUnitAttribute uAttr;


    aPosition = tAttr.create("position", "pos", MFnData::kVectorArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aPosition);mser;

    aColorPP = tAttr.create("color", "col", MFnData::kVectorArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aColorPP);mser;

    aStreamParam = tAttr.create("streamParam", "spm", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aStreamParam);mser;

    aStreamIdPP = tAttr.create("streamId", "sid", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aStreamIdPP);mser;

    aBreakPP = tAttr.create("break", "brk", MFnData::kDoubleArray);
    tAttr.setStorable(false);
    tAttr.setDisconnectBehavior(MFnAttribute::kReset);
    st = addAttribute(aBreakPP);mser;

    aGapThreshold = nAttr.create( "gapThreshold", "gt",MFnNumericData::kDouble );
    nAttr.setStorable(true);
    st =  addAttribute(aGapThreshold);  mser;

    aOutDummy = nAttr.create("outDummy", "odm", MFnNumericData::kInt, 0, &st); mser;
    nAttr.setWritable(false);
    nAttr.setStorable(false);
    nAttr.setHidden(true);
    st = addAttribute(aOutDummy); mser;

    st =attributeAffects(aPosition ,aOutDummy );mser;
    st =attributeAffects(aColorPP ,aOutDummy );mser;
    st =attributeAffects(aStreamIdPP ,aOutDummy );mser;
    st =attributeAffects(aStreamParam ,aOutDummy );mser;
    st =attributeAffects(aGapThreshold ,aOutDummy );mser;


    return MS::kSuccess;
}