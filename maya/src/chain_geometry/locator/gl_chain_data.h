
#ifndef glChainData_H
#define glChainData_H

#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>

#include <maya/MObject.h>
#include <chain_data.h>


class glChainData : public chainData {
public:
	glChainData();
	glChainData (
		const MVectorArray &pos,
		const MDoubleArray &streamId,
		const MDoubleArray &streamParam,
		MStatus *st
		);
	~glChainData();

	MStatus assignColors(const MVectorArray &arr);

	void glLines(
		MFloatVectorArray & start,
		MFloatVectorArray & end,
		MFloatVectorArray & col_start,
		MFloatVectorArray & col_end
		) const ;

	const MVector getColor (const int nIndex) const ;

private:

	const MVectorArray * m_pColor;

};

#endif


