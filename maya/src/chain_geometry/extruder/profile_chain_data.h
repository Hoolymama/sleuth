
#ifndef profileChainData_H
#define profileChainData_H
#include <map>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>

#include <maya/MObject.h>
#include <matrix_chain_data.h>

/*
PROFILE_PAIR is positions and uv values for one profile
*/
typedef std::pair<MPointArray, MVectorArray> PROFILE_PAIR;

/*
PROFILE_GROUP is a related set of profile curves. They will be extruded along the same chain and keep their position relative to each other.
*/
typedef std::vector<PROFILE_PAIR> PROFILE_GROUP;

/*
PROFILE_GROUP_MAP associates a profile group with an id so that a chain may specify the id of the profile group it wants to extrude.
*/
typedef std::map<int, PROFILE_GROUP>  PROFILE_GROUP_MAP;


class profileChainData : public matrixChainData {
public:
	profileChainData();
	profileChainData (
		const MVectorArray &pos,
		const MDoubleArray &streamId,
		const MDoubleArray &streamParam,
		MStatus *st
		);
	~profileChainData();

	void addProfile(int id, const MPointArray & vals);

	void extrude(  double uWidth,   double vLength, MObject &geom) ;

	MStatus assignUOffsets(const MDoubleArray &arr);

	const double getUOffset (const int nIndex) const ;



	MStatus assignVOffsets(const MDoubleArray &arr);

	const double getVOffset (const int nIndex) const ;




private:

	void normalizeProfileGroupUVs();

	void appendPoints(const MPointArray &curr_verts)  ;

	void sweepVertices(const chain &sweepChain, const MPointArray &profilePoints)  ;

	void createTube( int numPearls, int numProfilePoints)  ;

	void createRibbon(int numPearls) ;

	void  generateTubeUVs(double uWidth ,double vLength ,const chain &sweepChain, const MVectorArray & profileUVs)  ;

	void  generateRibbonUVs(double uWidth ,double vLength , const chain &sweepChain)  ;

	void  createTubeUVMapping(int numProfileUVs,int numPearls)  ;

	void  createRibbonUVMapping(int numPearls)  ;



	const PROFILE_GROUP * getProfileGroup(int id, MStatus * st)  const ;

	PROFILE_GROUP_MAP m_profileGroupMap;

	
	MPointArray m_vertices ;	
	MIntArray m_faceCounts;
	MIntArray m_connectivity;
	MIntArray m_uvCounts;
	MIntArray m_uvIds;
	MFloatArray m_uVals;
	MFloatArray m_vVals;
	int m_vertexIndex;
	int m_uvIndex;

	const MDoubleArray * m_pUOffset;
	const MDoubleArray * m_pVOffset;

};

#endif


