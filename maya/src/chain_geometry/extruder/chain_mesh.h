

#include <maya/MPointArray.h>
#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MStatus.h>

class chainMesh : public MPxNode
{
public:
	chainMesh();
	virtual           ~chainMesh();

	virtual MStatus   compute( const MPlug&, MDataBlock& );
	virtual void      postConstructor();

	static  void *    creator();
	static  MStatus   initialize();
	static  MTypeId   id;



	void calcDefaultProfile(
		int numProfilePoints, 
		double profileScale, 
		MPointArray & profileVertices) const ;

private:

	static MObject aPosition; // array of chain points
	static MObject aPhi;
	static MObject aStreamParam; // array of param values (how far along the chain)
	static MObject aStreamIdPP;
	static MObject aRadiusPP;
	static MObject aTwistPP;
	static MObject aBreakPP;
	static MObject aProfileCurveIdPP;
	static MObject aGapThreshold;
	static MObject aNumProfilePoints;
	static MObject aProfileScale;
	static MObject aProfileCurves;
	static MObject aCurveSet;

	static MObject aUWidth; 
	static MObject aUOffset;

	 static MObject aVLength;
	 static MObject aVOffset;

	static MObject aOutMesh;


};
