
#include <maya/MIOStream.h>
#include <math.h>
#include <maya/MPxNode.h>
#include <maya/MStatus.h>

#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPluginData.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnMeshData.h>
#include <maya/MFloatPoint.h>
#include <maya/MFnMesh.h>
#include <maya/MTime.h>

#include <mayaMath.h>
#include "errorMacros.h"
#include "jMayaIds.h"

#include <profile_chain_data.h>


#include "chain_mesh.h"


// Data from particle system
MObject chainMesh::aPosition; // array of chain point positions
MObject chainMesh::aPhi;  // array of rotations (axis/angle) associated with points
MObject chainMesh::aStreamParam;  // array of param values (how far along the chain)
MObject chainMesh::aStreamIdPP; // array of ids - what chain the point belongs to
MObject chainMesh::aRadiusPP; // radius - used to scale the extrusion at a chain point
MObject chainMesh::aTwistPP; // twist value along the chain's tangent
MObject chainMesh::aBreakPP; // should the extrusion break at this point
MObject chainMesh::aProfileCurveIdPP; // which profile curve should be extruded along this chain


MObject chainMesh::aGapThreshold; // a threshold distance value. If two points in a stream are further, then break the extrusion
MObject chainMesh::aNumProfilePoints; // if no profile curves attached, make a regular n-gon with this number of points.
MObject chainMesh::aProfileScale; // cross section global scale
MObject chainMesh::aCurveSet; // set of profile curves
MObject chainMesh::aProfileCurves; // compound attribute to hold many curve sets.

// UV Coord creation
MObject chainMesh::aUWidth; // 
MObject chainMesh::aUOffset; // 0.0 means whole width will occupy the same U value - good for tubes

MObject chainMesh::aVLength; //  for example - 0.2 means whole extrusion takes up 0.2 of V space
MObject chainMesh::aVOffset; // v coord offset

MObject chainMesh::aOutMesh;

const double PI_X2 = 6.283185306;
const double PI = 3.141592653;

MTypeId chainMesh::id( k_chainMesh );

chainMesh::chainMesh() {	}

chainMesh::~chainMesh()
{
}

void* chainMesh::creator()
{
	return new chainMesh();
}
void chainMesh::postConstructor( ){}


MStatus chainMesh::initialize()
{
	MStatus st;

	MString method("chainMesh::initialize");
	MFnNumericAttribute nAttr; 
	MFnTypedAttribute tAttr; 
	MFnCompoundAttribute cAttr; 
	MFnUnitAttribute uAttr;

	aPosition = tAttr.create("position", "pos", MFnData::kVectorArray);
    tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setCached(false);
	st = addAttribute(aPosition);mser;

	aPhi = tAttr.create("phi", "phi", MFnData::kVectorArray);
    tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setCached(false);
	st = addAttribute(aPhi);mser;

	aStreamParam = tAttr.create("streamParam", "spm", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aStreamParam);mser;

	aStreamIdPP = tAttr.create("streamId", "sid", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aStreamIdPP);mser;

	aRadiusPP = tAttr.create("radius", "rad", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aRadiusPP);mser;

	aTwistPP = tAttr.create("twist", "tws", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aTwistPP);mser;

	aBreakPP = tAttr.create("break", "brk", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aBreakPP);mser;

	aProfileCurveIdPP = tAttr.create("profileId", "prid", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aProfileCurveIdPP);mser;

	aGapThreshold = nAttr.create( "gapThreshold", "gt",MFnNumericData::kDouble );
	nAttr.setStorable(true);
	st =  addAttribute(aGapThreshold);  mser;

	aNumProfilePoints = nAttr.create("defaultProfilePoints","dpps", MFnNumericData::kLong);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(3);
	addAttribute( aNumProfilePoints );
	
	aProfileScale = nAttr.create("defaultProfileScale","dpsc", MFnNumericData::kDouble);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0f);
	addAttribute( aProfileScale );

	aCurveSet = tAttr.create("curveSet","cst", MFnData::kNurbsCurve );
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setArray(true);
	tAttr.setIndexMatters(false);

	aProfileCurves = cAttr.create("profileCurves","pcs");
	cAttr.addChild(aCurveSet);
	cAttr.setArray(true);
	cAttr.setIndexMatters(true);
	cAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aProfileCurves);

	aUWidth = nAttr.create("uWidth","uwid", MFnNumericData::kDouble);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0f);
	addAttribute( aUWidth );

	aUOffset = tAttr.create("uOffset", "uoff", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aUOffset);mser;

	aVLength = nAttr.create("vLength","vlen", MFnNumericData::kDouble);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0f);
	addAttribute( aVLength );

	aVOffset = tAttr.create("vOffset", "voff", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aVOffset);mser;

	aOutMesh = tAttr.create( "outMesh", "om", MFnData::kMesh );
	tAttr.setStorable(false);
	tAttr.setWritable(false);
	addAttribute( aOutMesh );

	attributeAffects (aPosition, aOutMesh);
	attributeAffects (aPhi, aOutMesh);
	attributeAffects (aRadiusPP, aOutMesh);
	attributeAffects (aStreamParam, aOutMesh);
	attributeAffects (aStreamIdPP, aOutMesh);
	attributeAffects (aGapThreshold, aOutMesh);
	attributeAffects (aCurveSet, aOutMesh); // is this needed?
	attributeAffects (aProfileCurves, aOutMesh);
	attributeAffects (aProfileCurveIdPP, aOutMesh);
	attributeAffects (aBreakPP, aOutMesh);
	attributeAffects (aTwistPP, aOutMesh);
	attributeAffects (aNumProfilePoints, aOutMesh);
	attributeAffects (aProfileScale, aOutMesh);

	attributeAffects (aUWidth, aOutMesh);
	attributeAffects (aUOffset, aOutMesh);

	attributeAffects (aVLength, aOutMesh);
	attributeAffects (aVOffset, aOutMesh);


	return MS::kSuccess;
}


// In the absence of explicit profile curve geometry, we make a regular polygon for the extrusion
void chainMesh::calcDefaultProfile(
	int numProfilePoints, 
	double profileScale, 
	MPointArray & profileVertices
) const 
{
	for (unsigned i = 0;i<numProfilePoints;i++) {
		double gap = ((i * PI_X2) + PI ) / double(numProfilePoints) ;
		double x = sin(gap) *profileScale *0.5;
		double y = cos(gap) *profileScale *0.5;
		profileVertices.append(MPoint(0, y, x)) ;
	}
}


MStatus chainMesh::compute( const MPlug& plug,  MDataBlock& data ) 
{ 
	MStatus st;

	MString method("chainMesh::compute");


	if(plug != aOutMesh) return MS::kUnknownParameter;

  // create mesh data structure for output
	MFnMeshData fnC;
	MObject outGeom = fnC.create(&st);mser;
	MFnMesh fnM;

	// function sets to extract data from the arrays
	MFnVectorArrayData fnV;
	MFnDoubleArrayData fnD;
	MDataHandle h;
	MObject d;

	unsigned pl = 0;

	h = data.inputValue(aPosition, &st);msert;
	d = h.data();
	st = fnV.setObject(d);msert;
	pl = fnV.length();
	const MVectorArray & pos = fnV.array();	


	// clean the mesh and ouput if no points
	if ( !pl) {
		fnM.create( 0, 0, MFloatPointArray(), MIntArray(), MIntArray(), outGeom, &st );  mser;
		MDataHandle hMesh = data.outputValue(aOutMesh, &st);
		hMesh.set(outGeom);
		data.setClean(aOutMesh);
		return MS::kUnknownParameter;	
	}

	
	h = data.inputValue(aStreamIdPP, &st);msert;
	d = h.data();
	st = fnD.setObject(d);msert;
	if (fnD.length() != pl) {st=MS::kUnknownParameter;msert;}
	const MDoubleArray & sid = fnD.array();	


	h = data.inputValue(aStreamParam, &st);msert;
	d = h.data();
	st = fnD.setObject(d);msert;
	if (fnD.length()  != pl) {st=MS::kUnknownParameter;msert;}
	const MDoubleArray & streamParam = fnD.array();	


	MVectorArray phi ;
	h = data.inputValue(aPhi, &st);
	if (! st.error()) {
		d = h.data();
		st = fnV.setObject(d);
		if (! st.error()) {
			if (fnV.length() == pl) {
				phi = fnV.array();	
			}	
		}
	}

	MDoubleArray rad ;
	h = data.inputValue(aRadiusPP, &st);
	if (! st.error()) {
		d = h.data();
		st = fnD.setObject(d);
		if (! st.error()) {
			if (fnD.length() == pl) {
				rad = fnD.array();	
			}	
		}
	}
	
	MDoubleArray twist ;
	h = data.inputValue(aTwistPP, &st);
	if (! st.error()) {
		d = h.data();
		st = fnD.setObject(d);
		if (! st.error()) {
			if (fnD.length() == pl) {
				twist = fnD.array();	
			}	
		}
	}

	MDoubleArray snip ;
	h = data.inputValue(aBreakPP, &st);
	if (! st.error()) {
		d = h.data();
		st = fnD.setObject(d);
		if (! st.error()) {
			if (fnD.length() == pl) {
				snip = fnD.array();	
			}	
		}
	}


	MDoubleArray pcid ;
	h = data.inputValue(aProfileCurveIdPP, &st);
	if (! st.error()) {
		d = h.data();
		st = fnD.setObject(d);
		if (! st.error()) {
			if (fnD.length() == pl) {
				pcid = fnD.array();	
			}	
		}
	}

	MDoubleArray uoff ;
	h = data.inputValue(aUOffset, &st);
	if (! st.error()) {
		d = h.data();
		st = fnD.setObject(d);
		if (! st.error()) {
			if (fnD.length() == pl) {
				uoff = fnD.array();	
			}	
		}
	}

	MDoubleArray voff ;
	h = data.inputValue(aVOffset, &st);
	if (! st.error()) {
		d = h.data();
		st = fnD.setObject(d);
		if (! st.error()) {
			if (fnD.length() == pl) {
				voff = fnD.array();	
			}	
		}
	}


	
	double gt = data.inputValue( aGapThreshold).asDouble();
	if (gt == 0.0) gt = 99999999.0;

	// double vcmax =data.inputValue( aVCoordLength).asDouble();
	// if (gt == 0.0) vcmax = 5.0;

	double uwid =data.inputValue( aUWidth).asDouble();
	double vlen =data.inputValue( aVLength).asDouble();

	int numProfilePoints = data.inputValue(aNumProfilePoints).asShort();
	if (numProfilePoints < 2) numProfilePoints = 2;
	double profileScale = data.inputValue(aProfileScale).asDouble();
	MPointArray profileVertices;
	calcDefaultProfile(numProfilePoints, profileScale, profileVertices);


	// make a map of MPointArrays representing profile curves.
 	profileChainData chains(pos,sid,streamParam,&st);msert;

	if (rad.length() == pl) chains.assignRadii(rad);
	if (snip.length() == pl) chains.assignSnips(snip);
	if (pcid.length() == pl) chains.assignGeometryIds(pcid);

	if (phi.length() == pl) chains.assignPhis(phi);
	if (twist.length() == pl) chains.assignTwists(twist);
	if (uoff.length() == pl) chains.assignUOffsets(uoff);
	if (voff.length() == pl) chains.assignVOffsets(voff);



	chains.addProfile(-1,profileVertices);

	MArrayDataHandle hac = data.inputArrayValue(aProfileCurves);
	unsigned nf = hac.elementCount();
	MObject dc;
	for(unsigned i = 0;i < nf; i++, hac.next()) {
		int id = hac.elementIndex(&st);
		MDataHandle hccompound = hac.inputValue();
		MArrayDataHandle haset = hccompound.child(aCurveSet);
		unsigned nsets = haset.elementCount();
		for(unsigned j = 0;j < nsets; j++, haset.next()) {

			MDataHandle hc = haset.inputValue();
			MPointArray pts;
			dc = hc.asNurbsCurveTransformed();
			MFnNurbsCurve fnCurve( dc, &st ); mser;
			fnCurve.getCVs(pts);
			unsigned plen = pts.length();
			if (plen > 1) {
				chains.addProfile(id,pts);
			}
		}
	}

	chains.makeChains(gt);
	chains.extrude(uwid, vlen, outGeom);

	MDataHandle hMesh = data.outputValue(aOutMesh, &st);
	hMesh.set(outGeom);
	data.setClean(aOutMesh);

	return MS::kSuccess;
}