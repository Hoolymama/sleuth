/*
profile_chain_data creates chains from particles and extrudes one or more profile curves along those chains.

To create the extrusion, we need coordinate systems along the chains. Therefore, this class is inherited from matrix_chain_data.


*/


#include <profile_chain_data.h>


#include <maya/MFnMesh.h>
profileChainData::profileChainData(){}

profileChainData::profileChainData (
     const MVectorArray &pos,
     const MDoubleArray &streamId,
     const MDoubleArray &streamParam,
     MStatus *st
) 
:matrixChainData(pos,streamId,streamParam,st),
m_vertices(), 
m_faceCounts(), 
m_connectivity(),
m_uVals(), 
m_vVals(),
m_uvCounts(), 
m_uvIds(),
m_vertexIndex(0),
m_uvIndex(0),
m_pUOffset(0),
m_pVOffset(0)
{}


profileChainData::~profileChainData(){}


/* set U offset info to each pearl */
MStatus profileChainData::assignUOffsets(const MDoubleArray &arr){
    unsigned pl = m_pearlArray.size();
    if (arr.length() != pl) return MS::kUnknownParameter;
    m_pUOffset = &arr;
    return MS::kSuccess;
}


const double profileChainData::getUOffset (const int nIndex) const 
{
    if (m_pUOffset && (nIndex < m_pUOffset->length()))
        return (*m_pUOffset)[nIndex];
    return 0.0;
}

/* set V offset info to each pearl */
MStatus profileChainData::assignVOffsets(const MDoubleArray &arr){
    unsigned pl = m_pearlArray.size();
    if (arr.length() != pl) return MS::kUnknownParameter;
    m_pVOffset = &arr;
    return MS::kSuccess;
}

const double profileChainData::getVOffset (const int nIndex) const 
{
    if (m_pVOffset && (nIndex < m_pVOffset->length()))
        return (*m_pVOffset)[nIndex];
    return 0.0;
}


/*
Add a profile curve to the correct PROFILE_GROUP within the PROFILE_GROUP_MAP
*/
void profileChainData::addProfile(int id, const MPointArray & vals){

    PROFILE_GROUP_MAP::iterator mapiter = m_profileGroupMap.find(id);
    if (mapiter == m_profileGroupMap.end()) {
        PROFILE_GROUP pgroup;
        m_profileGroupMap[id] = pgroup;
        mapiter = m_profileGroupMap.find(id);
    }
    MPointArray points;
    points.copy(vals);

    unsigned len = vals.length();
    MVectorArray uvs(len);
    for (int i = 0; i < len; ++i)
    {
        uvs[i] = MVector(points[i].z,points[i].y,0.0);
    }
    PROFILE_PAIR pr(points,uvs);
    mapiter->second.push_back(pr);
}


/*
UVs are added to a profile curve based on X and y values. So here we visit all profiles and normalize their UVs. 
*/
void profileChainData::normalizeProfileGroupUVs(){

    PROFILE_GROUP_MAP::iterator mapiter = m_profileGroupMap.begin();
    for (; mapiter!=m_profileGroupMap.end(); ++mapiter) {
        PROFILE_GROUP & pg = mapiter->second;
        PROFILE_GROUP::iterator giter = pg.begin();

        // these are the first u v values in the first array 
        double minu = giter->second[0].x;
        double maxu = minu;
        double minv = giter->second[0].y;
        double maxv = minv;

        // find extents of all vals of all curves
        double uRange, vRange;

        for (; giter!=pg.end(); ++giter) {
            MVectorArray & va = giter->second;
            unsigned len = va.length();
            for (int i = 0; i < len; ++i)
            {
                const double & u = va[i].x;
                const double & v = va[i].y;
                if (u < minu) {minu = u; } else if (u > maxu) {maxu = u;}
                if (v < minv) {minv = v;} else if (v > maxv) {maxv = v;}  
            }
        }
        uRange = maxu - minu;
        vRange = maxv - minv;

        // now we have the ranges, we can normalize
        giter = pg.begin();
        for (; giter!=pg.end(); ++giter) {
            MVectorArray & va = giter->second;
            unsigned len = va.length();
            for (int i = 0; i < len; ++i)
            {
                va[i].x = ((va[i].x)-minu) / uRange;
                va[i].y = ((va[i].y)-minv) / vRange;
            }
        }
    }
}

/*
return the set of profile curves with the given Id
*/
const PROFILE_GROUP * profileChainData::getProfileGroup(int id, MStatus * st)  const {
    *st = MS::kSuccess;
    PROFILE_GROUP_MAP::const_iterator mapiter = m_profileGroupMap.find(id);
    if (mapiter != m_profileGroupMap.end()) {
        return &(mapiter->second); 
    } else {
        mapiter = m_profileGroupMap.find(-1);
        if (mapiter != m_profileGroupMap.end()) {
            return &(mapiter->second); 
        } else {
           *st = MS::kFailure;
           return 0;
       }
   }
}

/*
Loop though all the chains - for each chain select the correct set of profile curves and extrude each one. 
*/
void profileChainData::extrude(double uWidth ,double vLength, MObject &geom) 
{
    MStatus st;

    normalizeProfileGroupUVs();

    CHAIN_LIST::const_iterator itCurrChain = m_chainList.begin();
    int vertexId = 0;
    while (itCurrChain != m_chainList.end()) {

        const PROFILE_GROUP * pProfiles = getProfileGroup((*itCurrChain)->geometryId(), &st);
        if (st.error()) return;

        // loop through profile group
        PROFILE_GROUP::const_iterator itCurrProfile = pProfiles->begin();
        for (; itCurrProfile != pProfiles->end(); ++itCurrProfile)
        {

            // create geometry
            const MPointArray & profilePoints = itCurrProfile->first;
            int nPoints = profilePoints.length();
            const chain & cur_chain = **itCurrChain;
            int numPearls = cur_chain.length()  ;
            sweepVertices(cur_chain,profilePoints);

            if (nPoints > 2) {
                createTube(numPearls, nPoints);
            } else {
                createRibbon(numPearls);
            }

            // now create UV mapping
            const MVectorArray & profileUVs = itCurrProfile->second;
            int nUVs = profileUVs.length();

            if (nUVs > 2) {
                generateTubeUVs(uWidth,vLength,cur_chain,profileUVs);
                createTubeUVMapping(nUVs,numPearls);
            } else {
                generateRibbonUVs(uWidth,vLength,cur_chain);
                createRibbonUVMapping(numPearls);
            }
        }

        itCurrChain++;
    }
    MFnMesh fnMesh;

    fnMesh.create( 
        m_vertices.length(), 
        m_faceCounts.length(), 
        m_vertices, 
        m_faceCounts, 
        m_connectivity, geom, &st 
    );  mser;


    st = fnMesh.setUVs(m_uVals, m_vVals);  mser; 
    st = fnMesh.assignUVs ( m_uvCounts, m_uvIds ); mser; 


}

/*
In the case where the profile shape has more than 2 vertices we treat it as a tube. We want to close the extrusion in U direction. Therefore, the first vertex will have 2 U values associated with it. (0 and 1 - or actually min AND max)
 */

 void  profileChainData::generateTubeUVs(double uWidth ,double vLength, const chain &sweepChain, const MVectorArray & profileUVs)  
 {


    int metaId = (sweepChain.pearls()[0])->metaId();
    const double &uoff = getUOffset(metaId);
    const double &voff = getVOffset(metaId);

    int last = sweepChain.length() -1;
    float arclen = (sweepChain.pearls()[last])->distance();

    int numProfileUVs =  profileUVs.length();

    for (int i = 0; i < numProfileUVs; ++i)
    {
        //  make V zero and U has offset and width applied
        m_uVals.append(float((profileUVs[i].x*uWidth) + uoff ));
        m_vVals.append(float(voff));
    }

    // const double vcmaxRecip = 1.0 / vcmax;
    PEARL_ARRAY::const_iterator curr_pearl = sweepChain.pearls().begin();


    double uRange  =  double(numProfileUVs) ;

    float vVal ;
    while (curr_pearl !=  sweepChain.pearls().end()) {
        vVal =(((*curr_pearl)->distance() / arclen) * vLength) + voff;

        for (int i = 0; i <= numProfileUVs; ++i)
        {
            m_uVals.append( ((i / uRange ) * uWidth) + uoff);
            m_vVals.append(vVal);     
        }
        curr_pearl++;
    }
    for (int i = 0; i < numProfileUVs; ++i)
    {
        m_uVals.append(float((profileUVs[i].x*uWidth) + uoff ));
        //  make it same as last v and U has offset and width applied
        m_vVals.append(vVal);
    }
 }
/*
In the case where the profile shape has only 2 vertices we treat it as a ribbon. UV generation is on a patch with 1 UV val per vertex
*/
 void  profileChainData::generateRibbonUVs(double uWidth ,double vLength,const chain &sweepChain)  
 {   
    int metaId = (sweepChain.pearls()[0])->metaId();
    const double &uoff = getUOffset(metaId);
    const double &voff = getVOffset(metaId);

    int last = sweepChain.length() -1;
    float arclen = (sweepChain.pearls()[last])->distance();

    PEARL_ARRAY::const_iterator curr_pearl =  sweepChain.pearls().begin();
    
    float vVal ;
    while (curr_pearl !=  sweepChain.pearls().end()) {
       vVal =(((*curr_pearl)->distance() / arclen) * vLength) + voff;
        m_uVals.append(uoff);
        m_vVals.append(vVal);
        m_uVals.append((uWidth) + uoff);
        m_vVals.append(vVal);
        curr_pearl++;
    }

}
/*
create the UV information for the mesh as a tube. 
*/
 void  profileChainData::createTubeUVMapping(int numProfileUVs, int numPearls)  
 {

    // start cap
    m_uvCounts.append(numProfileUVs);
    for (unsigned k = 0;k<numProfileUVs;k++) {
        int the_id =  m_uvIndex + ((numProfileUVs-1) - k);
        m_uvIds.append(the_id) ;
    }
    m_uvIndex +=numProfileUVs;
 
 
    for (unsigned j = 0;j<(numPearls-1);j++) {
        
        // for example - suppose numProfileUVs is 5
        // first we make faces starting with verts 0 -> 4 
        // this means the second vert of those faces is 1 -> 5 and so on
        // then we close the last vert (4) with the first (0)
 
        for (unsigned k = 0;k<(numProfileUVs);k++) {
            m_uvCounts.append(4);
            m_uvIds.append(m_uvIndex);
            m_uvIds.append(m_uvIndex+1);
            m_uvIds.append(m_uvIndex+numProfileUVs+2);
            m_uvIds.append(m_uvIndex+numProfileUVs+1);

            m_uvIndex+=1; // work our way round.
        }
        m_uvIndex+=1; // jump up one step.
    }
    m_uvIndex+=(numProfileUVs+1); // jump off the end

    // end cap
    m_uvCounts.append(numProfileUVs);
    for (unsigned k = 0;k<numProfileUVs;k++) {
        m_uvIds.append(m_uvIndex);
        m_uvIndex ++;
    }
 }

// create the UV information for the mesh as a ribbon.
 void  profileChainData::createRibbonUVMapping(int numPearls)  
 {

    for (unsigned j = 0;j<(numPearls-1);j++) {
        m_uvCounts.append(4);
        m_uvIds.append(m_uvIndex);
        m_uvIds.append(m_uvIndex+1);
        m_uvIds.append(m_uvIndex+3);
        m_uvIds.append(m_uvIndex+2);
        m_uvIndex+=2;
    }
    m_uvIndex+=2; // jump off the end
 }



void profileChainData::appendPoints(const MPointArray &curr_verts)  {
     unsigned len = curr_verts.length();
    for (int i = 0; i < len; ++i) {m_vertices.append(curr_verts[i]);}
}

/*
Extrude a single profile along a single chain. We only generate vertex positions here. 
TODO: scale the xform matrix with radius value in the constructor.
*/
void profileChainData::sweepVertices(const chain &sweepChain, const MPointArray &profilePoints)  
{
    MPointArray curr_verts;
    PEARL_ARRAY::const_iterator  curr_pearl;
    curr_pearl = sweepChain.pearls().begin();
    while (curr_pearl != sweepChain.pearls().end()) {
        double rad = (*curr_pearl)->radius();
        curr_verts.copy(profilePoints);
        const MMatrix & thismat = (*curr_pearl)->matrix();
        mayaMath::transformPoints(thismat,rad,curr_verts);
        appendPoints(curr_verts);
        curr_pearl++;
    }
}


/*
Create capped extrude polygon connectivity. We only need to know the number of pearls in the chain, and the number of profile verts.

First and last poly (caps) will have numProfilePoints verts. All the others will be quads.
*/
void profileChainData::createTube(
    int numPearls,
    int numProfilePoints
)  
{

    // start cap
    m_faceCounts.append(numProfilePoints);
    for (unsigned k = 0;k<numProfilePoints;k++) {
        int the_id =  m_vertexIndex + ((numProfilePoints-1) - k);
        m_connectivity.append(the_id);
    }

    // quads of the tube
    for (unsigned j = 0;j<(numPearls-1);j++) {
        
        // for all but the last quad in U, we connect to the next verts around.
        for (unsigned k = 0;k<(numProfilePoints-1);k++) {
            m_faceCounts.append(4);
            m_connectivity.append(m_vertexIndex);
            m_connectivity.append(m_vertexIndex+1);
            m_connectivity.append(m_vertexIndex+numProfilePoints+1);
            m_connectivity.append(m_vertexIndex+numProfilePoints);

            m_vertexIndex+=1;
        }
        // for the last quad, to close the tube, we connect to the first verts in U
        m_faceCounts.append(4);
        m_connectivity.append(m_vertexIndex);
        m_connectivity.append((m_vertexIndex+1) - numProfilePoints);
        m_connectivity.append(m_vertexIndex+1);
        m_connectivity.append(m_vertexIndex+numProfilePoints);
        m_vertexIndex +=1;
    }

    // end cap
    m_faceCounts.append(numProfilePoints);
    for (unsigned k = 0;k<(numProfilePoints);k++) {
        m_connectivity.append(m_vertexIndex + k);
    }

    m_vertexIndex +=numProfilePoints;  // jump off the end
}


/*
Create ribbon connectivity in the case where number of profile points is 2. We only need to know the number of pearls in the chain.
*/
void profileChainData::createRibbon(int numPearls)  {
    for (unsigned j = 0;j<(numPearls-1);j++) {
        m_faceCounts.append(4);
        m_connectivity.append(m_vertexIndex);
        m_connectivity.append(m_vertexIndex+1);
        m_connectivity.append(m_vertexIndex+3);
        m_connectivity.append(m_vertexIndex+2);

        m_vertexIndex +=2;
    }
    m_vertexIndex +=2;
}

