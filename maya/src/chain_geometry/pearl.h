

#ifndef pearl_H
#define pearl_H

#include <maya/MIOStream.h>
#include <vector>

#include <math.h>
#include <maya/MVector.h>

#include <errorMacros.h>
#include <mayaMath.h>

class pearl{

public:
	pearl();

	pearl(
		const MVector & p, 
		double streamParam,
		int streamId,
		int metaId	
	);

	~pearl();

	const MVector & position() const ;
	const MMatrix & matrix() const ;
	const double & streamParam() const ;
	const float & distance() const ;
		
	const int & streamId() const;
	const int & snip() const;
	const int & metaId() const;
	const int & geometryId() const;

	const double & radius() const ;



	void setMatrix(const MMatrix & m);
	void setPosition(const MVector & v);
	void setStreamParam(const double & v);
	void setDistance(const float & v);
	void setStreamId(int v);
	void setSnip(int v);	
	void setMetaId(int v);
	void setGeometryId(int v);
	// 

	void setRadius(const double & v);

private:
		
	MVector m_position;
	double m_streamParam; 
	int m_snip;
	int m_streamId;
	int m_metaId;
	int m_geometryId;
	float m_distance;
	MMatrix m_matrix;
	
	double m_radius; 


};


typedef std::vector<pearl*> PEARL_ARRAY;

#endif