/*
matrix_chain_data extends chain_data with transformation matrices along the chain. Matrices are required for skinning or extruding geometry. 

User can provide arrays of phi (a vector that encodes rotation as axis/angle) and twists. 
*/


#include <matrix_chain_data.h>


#include <maya/MFnMesh.h>
matrixChainData::matrixChainData(){}

matrixChainData::matrixChainData (
     const MVectorArray &pos,
     const MDoubleArray &streamId,
     const MDoubleArray &streamParam,
     MStatus *st
) 
:chainData(pos,streamId,streamParam,st),
m_pPhi(0),
m_pTwist(0)
{}


matrixChainData::~matrixChainData(){}


MStatus matrixChainData::assignPhis(const MVectorArray &arr){
    unsigned pl = m_pearlArray.size();
    if (arr.length() != pl) return MS::kUnknownParameter;
    m_pPhi = &arr;
    return MS::kSuccess;
}

MStatus matrixChainData::assignTwists(const MDoubleArray &arr){
    unsigned pl = m_pearlArray.size();
    if (arr.length() != pl) return MS::kUnknownParameter;
    m_pTwist = &arr;
    return MS::kSuccess;
}

const double matrixChainData::getTwist (const int nIndex) const 
{
    if (m_pTwist && (nIndex < m_pTwist->length()))
        return (*m_pTwist)[nIndex];
    return 0.0;

}

const MVector matrixChainData::getPhi (const int nIndex) const 
{
    if (m_pPhi && (nIndex < m_pPhi->length()))
        return (*m_pPhi)[nIndex];
    return MVector::zero;
}

/*
call super::makeChains, then calculate matrices from phi (axis/angle) and twist values.
*/
MStatus matrixChainData::makeChains(double gap){
    MStatus st;
    st = chainData::makeChains(gap);
    if (st.error()) return st;


    CHAIN_LIST::iterator itCurrChain = m_chainList.begin();
    while (itCurrChain != m_chainList.end()) {

        const chain & curr_chain = **itCurrChain;

        int metaId = ((*itCurrChain)->pearls()[0])->metaId();
        const MVector &phi = getPhi(metaId);
        double twist = getTwist(metaId);

        (*itCurrChain)->setDistances() ; 
        (*itCurrChain)->setMatrices(phi,twist) ; 
        itCurrChain++;
    }
    return MS::kSuccess;
}

/*
 call makeChains, then calculate radii along the chain from a ramp lookup
*/ 
MStatus matrixChainData::makeChains(double gap,  MRampAttribute &rampAttr){
    MStatus st;
    st = matrixChainData::makeChains(gap);msert;

    CHAIN_LIST::iterator itCurrChain = m_chainList.begin();
    while (itCurrChain != m_chainList.end()) {
        (*itCurrChain)->applyRadiusRamp(rampAttr) ; 
        itCurrChain++;
    }

    return MS::kSuccess;
}




