/*
chain_data is a base class used for building some geometry along a set of chains that are defined by arrays of partcles. It is responsible for turning an unstructured array of particles into an array of chains.

chain_data is a base class and has subclasses used for building three diferent types of geometry.

gl_chain_data: generates enough information to build openGL lines along the chains.

matrix_chain_data: calculates transformations along the chains and is used to deform 3d meshes by warping them along the chain. 

profile_chain_data: inherits from matrix_chain_data and is used to build an extrusion based on a set of profile curves.

----

*/


#include <math.h>
#include <algorithm>
#include <utility>
#include <chain_data.h>
#include <errorMacros.h>
#include <maya/MMatrix.h>
#include <mayaMath.h>
#include <pearl.h>

/*
Since particles may arrive in any order, we need to sort the arrays of pearls based on which stream they belong to (where emitted from), and how far along the stream they are (age)
*/
struct comp_struct {
    bool operator() (pearl * a, pearl * b) { 
        if (a->streamId()  < b->streamId() ) return true;
        if (b->streamId()  < a->streamId() ) return false;
        if (a->streamParam()  < b->streamParam() ) return true;
        if (b->streamParam()  < a->streamParam() ) return false;
        return false;
    }
} compare_pearls;

chainData::chainData(){}


/*
constructor makes pearls (chain links) from arrays of data from the particle system.
*/
chainData::chainData (
    const MVectorArray &pos,
    const MDoubleArray &streamId,
    const MDoubleArray &streamParam,
    MStatus *st
    ){    

    *st = MS::kSuccess;
    unsigned pl = pos.length();

    if (streamId.length() != pl) {*st = MS::kUnknownParameter;return;}
    if (streamParam.length() != pl) {*st = MS::kUnknownParameter;return;}

    for (int i = 0; i < pl; ++i)
    {
        int nStreamId = int(streamId[i]+0.5);
        pearl * tp = new pearl(pos[i],streamParam[i],nStreamId,i);
        m_pearlArray.push_back(tp);
    }
}

chainData::~chainData(){
    PEARL_ARRAY::iterator piter = m_pearlArray.begin();
    while (piter != m_pearlArray.end()) {
        if (*piter) {
            delete *piter;
            *piter = 0;
        }
        piter++;
    }

    CHAIN_LIST::iterator citer = m_chainList.begin();
    while (citer != m_chainList.end()) {
        if (*citer) {
            delete *citer;
            *citer = 0;
        }
        citer++;
    }
}


MStatus chainData::assignGeometryIds(const MDoubleArray &arr){
    unsigned pl = m_pearlArray.size();
    if (arr.length() != pl) return MS::kUnknownParameter;
    PEARL_ARRAY::iterator iter = m_pearlArray.begin();
    unsigned i = 0;
    for (; iter != m_pearlArray.end(); ++iter, ++i) 
        (*iter)->setGeometryId(int(arr[i]));

    return MS::kSuccess;
}


MStatus chainData::assignSnips(const MDoubleArray &arr){
    unsigned pl = m_pearlArray.size();
    if (arr.length() != pl) return MS::kUnknownParameter;
    PEARL_ARRAY::iterator iter = m_pearlArray.begin();
    unsigned i = 0;
    for (; iter != m_pearlArray.end(); ++iter, ++i) 
        (*iter)->setSnip(int(arr[i]));

    return MS::kSuccess;
}

MStatus chainData::assignRadii(const MDoubleArray &arr) {
    unsigned pl = m_pearlArray.size();
    if (arr.length() != pl) return MS::kUnknownParameter;
    PEARL_ARRAY::iterator iter = m_pearlArray.begin();
    unsigned i = 0;
    for (; iter != m_pearlArray.end(); ++iter, ++i) 
        (*iter)->setRadius(arr[i]);
    return MS::kSuccess;
}


/*
This method arranges the pearls into chains. It sorts them based on streamId and age. Also, if a particular pearl has been flagged explicitly as a break, or if the gap between it and the next pearl is beyond the gap threshold, a new chain is started.
*/
MStatus chainData::makeChains(double gap){

    std::sort(m_pearlArray.begin(), m_pearlArray.end(), compare_pearls);

    MStatus st;

    PEARL_ARRAY::const_iterator piter = m_pearlArray.begin();

    if (piter == m_pearlArray.end()) return MS::kUnknownParameter;

    chain * cp = 0;


    int lastStreamId = -1;
    double lastStreamParam = (*piter)->streamParam();
    int lastSnip = 0;
    int pearlInChain = 0;
    while (piter != m_pearlArray.end()) {
        bool newChain = false;

        // should we start a new chain
        if ((*piter)->streamId() != lastStreamId) {
            newChain = true;
        } else if (( (*piter)->streamParam() - lastStreamParam )> double(gap)) {
            newChain = true;
        } else if (lastSnip) {
            newChain = true;
        } 

        if (newChain) {
            cp = new chain();
            m_chainList.push_back(cp);
        }

        cp->appendPearl((*piter));


        lastStreamId = (*piter)->streamId();
        lastStreamParam = (*piter)->streamParam();
        lastSnip = (*piter)->snip();

        piter++;
    }

    // delete all the chains that have less than 2 pearls because 
    // we wont do anything with a single pearl. It is not a chain.
    CHAIN_LIST::iterator itCurrChain = m_chainList.begin();

    while (itCurrChain != m_chainList.end()) {
        if ((*itCurrChain)->length() < 2)  {
            delete *itCurrChain;
            *itCurrChain = 0;
            itCurrChain = m_chainList.erase(itCurrChain);
        } else {
            itCurrChain++;
        }
    }

    return MS::kSuccess;
}


