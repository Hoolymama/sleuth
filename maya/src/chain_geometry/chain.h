
#ifndef chain_H
#define chain_H

#include <maya/MIOStream.h>
#include <maya/MRampAttribute.h>
#include <list>
#include <pearl.h>

class chain{
	
public:
	chain();

	~chain();

	const PEARL_ARRAY & pearls() const;

	int stream() const;

	int length() const;

	void appendPearl(pearl * p);

	int geometryId() const ;

	double arcLength() const ;

	int binarySearch(float x) const;

	MFloatPoint interpolatedTransformation(const MFloatPoint &p, int i) const ;

	void setDistances()  ;

	void applyRadiusRamp(  MRampAttribute &rampAtt)  ;

	void setMatrices(const MVector &phi, double twist);
private:

	PEARL_ARRAY m_pearlArray;	
};

typedef std::list<chain * > CHAIN_LIST;

#endif

