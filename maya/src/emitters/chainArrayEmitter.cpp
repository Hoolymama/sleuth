
#include <maya/MIOStream.h>
#include <math.h>
//#include <stdlib.h>

#include "chainArrayEmitter.h"

#include "lookup.h"
#include "attrUtils.h"
#include <maya/MDataHandle.h>

#include <maya/MGlobal.h>
#include <maya/MPlugArray.h>

#include <maya/MQuaternion.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>

#include <maya/MFnDependencyNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnAnimCurve.h>

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MArrayDataBuilder.h>

#include "lookup.h"
#include "attrUtils.h"
#include "mayaFX.h"
#include "mayaMath.h"
#include "jMayaIds.h"


const double EPSILON = 0.000001;

MObject chainArrayEmitter::aSamplePoints;
MObject chainArrayEmitter::aSampleVelocities;
MObject chainArrayEmitter::aSampleMasses;
MObject chainArrayEmitter::aSampleDeltaTime;
MObject chainArrayEmitter::aSampleFieldData;
MObject chainArrayEmitter::aForces;

MObject chainArrayEmitter::aInPoints;
MObject chainArrayEmitter::aInVelocities;
MObject chainArrayEmitter::aInAccelerations;

MObject chainArrayEmitter::aInRGB;
MObject chainArrayEmitter::aInRadius;
MObject chainArrayEmitter::aInAge;
MObject chainArrayEmitter::aInChainId;
MObject chainArrayEmitter::aInData;

MObject chainArrayEmitter::aForceRateRemap;
MObject chainArrayEmitter::aVelocityRateRemap;
MObject chainArrayEmitter::aAccelerationRateRemap;

MObject chainArrayEmitter::aAccelerationRate;
MObject chainArrayEmitter::aVelocityRate;
MObject chainArrayEmitter::aForceRate;
MObject chainArrayEmitter::aRateCap;

MObject chainArrayEmitter::aInheritAcceleration;
MObject chainArrayEmitter::aInheritVelocity;
MObject chainArrayEmitter::aInheritForce;

MObject chainArrayEmitter::aFacingRemap;

MTypeId chainArrayEmitter::id(  k_chainArrayEmitter);

chainArrayEmitter::chainArrayEmitter(){}
chainArrayEmitter::~chainArrayEmitter(){}

void *chainArrayEmitter::creator()
{
	return new chainArrayEmitter;
}





int chainArrayEmitter::distributeOverTime(
	double rate, int rateCap, double dt,
	const MVector &p, const MVector &v , 
	const MVector &inheritVec, const MVector &dirVec, 
	double speed, double speedRandom, double spread,
	MVectorArray &outPos, 
	MVectorArray &outVel, 
	MDoubleArray &outTim
	) {


	const double  PI  = 3.1415927;
	const double  _2PI = 2.0 * PI;
	
	// stuff needed for spread 
	//////////////////////////////////
	double rot_min = cos(spread*PI);
	double rot_t, rot_w, rot_z;
	double rot_range = 1.0 - rot_min;
	//////////////////////////////////


	unsigned int nRate = mayaMath::randCount(rate);
	if (nRate > rateCap) nRate=rateCap;
	
	MVector p0 = p-(v * dt);
	MVector deltaPos(p - p0);

	const MVector newVel((dirVec + inheritVec ) * speed);


	for(unsigned i = 0; i < nRate; ++i) {

		double sweep = (i+0.5) / double(nRate);	

		MVector sweepPos = p0 + (deltaPos * sweep);
		MVector sweepVel = newVel;
		double mag = sweepVel.length();
		if (speedRandom > 0.0) {
			mag = mag *  (1.0 + (speedRandom * 2.0 * (drand48() -0.5)));
		}

		if (spread > 0.0) {
			MVector tmp;
			MQuaternion q(MVector::zAxis, sweepVel);
			rot_z = (drand48() * rot_range) + rot_min;
			rot_t = drand48() * _2PI;
			rot_w = (sqrt( 1.0 - rot_z*rot_z )) ; // * mag;
			tmp.z = rot_z ; // * mag;
			tmp.x = rot_w * cos( rot_t );
			tmp.y = rot_w * sin( rot_t );
			sweepVel = tmp.rotateBy(q);
		}	else {
			sweepVel.normalize();
		}
		sweepVel = sweepVel * mag;


		outVel.append(sweepVel);
		outPos.append( sweepPos + ((1.0 - sweep) * dt * sweepVel)) ;	
		outTim.append( sweep);
		//triId.append(tri);

	}
	return nRate;
}




// void chainArrayEmitter::spreadVectors(const MVector & dir,double spread, MVectorArray & newVels){
// 	const double  PI  = 3.14159;
// 	const double  _2PI = 2.0 * PI;
// 	//const double  PI_2 =   PI * 0.5;
// 	unsigned int n = newVels.length();

// 	// make quaternions which rotate Z axis  back to dir
// 	MQuaternion q(MVector::zAxis, dir);
// 	double vl = dir.length();
// 	double min = cos(spread*PI);
// 	double t, w, z;
// 	double range = 1.0 - min;
// 	MVector v;
// 	for (;n;n--) {
// 		z = (drand48() * range) + min;

// 		t = drand48() * _2PI;
// 		w = (sqrt( 1.0 - z*z )) * vl;
// 		v.z = z * vl;
// 		v.x = w * cos( t );
// 		v.y = w * sin( t );
// 		newVels.set(v.rotateBy(q),(n-1));	
// 	}
// }

// unsigned int chainArrayEmitter::randomDistrib(
// 	double f,
// 	const MPoint &p1,
// 	const MPoint &p2,
// 	MVectorArray &pos,
// 	MDoubleArray &tim
// 	)
// {
// 	// generate n random points on a line p1 p2 
// 	// and append to the MVectorArray whose reference is passed in

// 	//double s;

// 	// The fractional part of f is the probability of an extra particle being
// 	// emitted.
// 	unsigned int nPoints = int(f);
// 	nPoints += drand48() < (f - double(nPoints)) ? 1 : 0;

// 	for(unsigned int n = 0; n < nPoints; n++){
// 		double sweep = ( (double)n + drand48() ) / (double)nPoints;
// 		pos.append((p1*(1-sweep))  + (p2*sweep)  );		
// 		tim.append(sweep);		
// 	}
// 	return nPoints;
// }



// void chainArrayEmitter::randSpeeds(double randomSpeed, MVectorArray & vels){
// 	unsigned int n = vels.length();
// 	unsigned int i;
// 	for (i=0;i<n;i++){
// 		float randFactor = float(drand48() * randomSpeed);
// 		MVector &v = vels[i];
// 		//	vels[i] += MVector(drand48() ,drand48() ,drand48() ).normal() * drand48() * randomSpeed;
// 		v += v.normal() * randFactor;
// 	}
// }


MStatus chainArrayEmitter::compute(const MPlug& plug, MDataBlock& block)
{

	MStatus st;
	///////////////////////////////////////////////////////////////////
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );


	// has no effect
	if ( block.inputValue( state ).asShort() == 1 ) return MS::kSuccess;


	int multiIndex = plug.logicalIndex( &st);mser;
	bool beenFull = isFullValue( multiIndex, block );
	long seedVal = seedValue( multiIndex, block );
	if( beenFull )return( MS::kSuccess );

	MArrayDataHandle hOutArray = block.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &st );mser;

	// check time and isFull
	///////////////////////////////////////////////////////////////////
	MTime cT = currentTimeValue( block );
	MTime sT = startTimeValue( multiIndex, block );
	MTime dT = deltaTimeValue( multiIndex, block );
	if( (cT <= sT) || (dT <= 0.0) )
	{
		hOut.set( dOutput );
		block.setClean( plug );
		return( MS::kSuccess );
	}
	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////
	MFnDependencyNode thisNodeFn(thisMObject());
	MObject  aSpread = thisNodeFn.attribute(MString("spread"));
	MObject  aSpeedRandom = thisNodeFn.attribute(MString("speedRandom"));
	// MObject  aRatePP = thisNodeFn.attribute(MString("ratePP"));
	// MObject  aUseRatePP = thisNodeFn.attribute(MString("useRatePP"));
	// MDoubleArray ratePP = MFnDoubleArrayData(block.inputValue(aRatePP).data()).array();

	// position and velocity arrays to append new particle data.

	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MVectorArray outCol = fnOutput.vectorArray("parentRgb", &st);mser;
	MDoubleArray outRad = fnOutput.doubleArray("parentRadius", &st);mser;
	MDoubleArray outTim = fnOutput.doubleArray("timeInStep", &st);mser;
	MDoubleArray outAge = fnOutput.doubleArray("parentAge", &st);mser;
    MDoubleArray outChainId = fnOutput.doubleArray("chainId", &st);mser;

	// Get attributes.
	// bool useRatePP = block.inputValue(aUseRatePP).asBool();
	double rate = doubleValue( block, mRate);
	double speed = doubleValue( block, mSpeed );
	double speedRandom = doubleValue( block, aSpeedRandom );
	// double emitterId = doubleValue( block, aEmitterId );
	double spread = doubleValue( block, aSpread );

	int rateCap = int(block.inputValue(aRateCap).asInt());

	double accelerationRate 	 = doubleValue( block,aAccelerationRate );
	double velocityRate 		 = doubleValue( block,aVelocityRate);
	double forceRate			 = doubleValue( block,aForceRate);
	double inheritAcceleration   = doubleValue( block,aInheritAcceleration );
	double inheritVelocity  	 = doubleValue( block,aInheritVelocity );
	double inheritForce 		 = doubleValue( block,aInheritForce);

	if (spread > 1 ) spread = 1;
	if (spread < 0 ) spread = 0;

	MVector dirV = vectorValue( block, mDirection );

	///////////////////////////////////////////////////////////////////
	MDataHandle hInData = block.inputValue( aInData, &st ); mser;
	// int count = hInArray.elementCount(&st); mser;
	// if (!count) {
	// 	return MS::kFailure;
	// }
	int pl = 0;
	int vl= 0;
	int al = 0;
	int fl= 0;
	int rl= 0;
	int cl= 0;
	int agl= 0;
	int rdl= 0;
	int idl= 0;	
	//int idl= 0;


	// Get facing curve stuff
	//////////////////////////////////////////////////////////
	lookup facingLookup;
	MFnAnimCurve facingAniFn; 
	st = getAniCurveFn(thisMObject(), aFacingRemap, facingAniFn);
	if (!(st.error())) {
		facingLookup.create(facingAniFn, 20);
	} 

	// Get rate lookup stuff
	//////////////////////////////////////////////////////////
	lookup forceRateRemap;
	MFnAnimCurve forceRateRemapAniFn; 
	st = getAniCurveFn(thisMObject(), aForceRateRemap, forceRateRemapAniFn);
	if (!(st.error())) {
		forceRateRemap.create(forceRateRemapAniFn, 20);
	} 

	lookup velocityRateRemap;
	MFnAnimCurve velocityRateRemapAniFn; 
	st = getAniCurveFn(thisMObject(), aVelocityRateRemap, velocityRateRemapAniFn);
	if (!(st.error())) {
		velocityRateRemap.create(velocityRateRemapAniFn, 20);
	} 

	lookup accelerationRateRemap;
	MFnAnimCurve accelerationRateRemapAniFn; 
	st = getAniCurveFn(thisMObject(), aAccelerationRateRemap, accelerationRateRemapAniFn);
	if (!(st.error())) {
		accelerationRateRemap.create(accelerationRateRemapAniFn, 20);
	} 

	////////////////////////////////////////////
	srand48(seedVal);
	

	////////////////////////////////////////////////
	////////////////////////////////////////////////

	MDataHandle hPoints = hInData.child( aInPoints );
	MObject dPoints = hPoints.data();
	MFnVectorArrayData fnPoints( dPoints );
	MVectorArray points = fnPoints.array( &st );mser;	
	pl = points.length();
	// if (!pl) continue;

	MVectorArray forces, velocities, accelerations,colors;
	MDoubleArray ages,normSpeed,radii, chainIds;


	MDataHandle hVelocities = hInData.child( aInVelocities );
	MObject dVelocities = hVelocities.data();
	MFnVectorArrayData fnVelocities( dVelocities );
	velocities = fnVelocities.array( &st );mser;

	MDataHandle hAccelerations = hInData.child( aInAccelerations );
	MObject dAccelerations = hAccelerations.data();
	MFnVectorArrayData fnAccelerations( dAccelerations );
	accelerations = fnAccelerations.array( &st );//mser;	

	MDataHandle hColors = hInData.child( aInRGB );
	MObject dColors= hColors.data();
	MFnVectorArrayData fnColors( dColors );
	colors = fnColors.array( &st );//mser;	

	MDataHandle hRadius = hInData.child( aInRadius );
	MObject dRadius= hRadius.data();
	MFnDoubleArrayData fnRadius( dRadius );
	radii = fnRadius.array( &st );//mser;	

	MDataHandle hAge = hInData.child( aInAge );
	MObject dAge= hAge.data();
	MFnDoubleArrayData fnAge( dAge );
	ages = fnAge.array( &st );//mser;	

	MDataHandle hChainId = hInData.child( aInChainId );
	MObject dChainId= hChainId.data();
	MFnDoubleArrayData fnChainId( dChainId );
	chainIds = fnChainId.array( &st );//mser;	

	if (forceRate || inheritForce) {

		MDoubleArray masses(pl, 1.0);
		MObject thisNode = thisMObject();
		st = mayaFX::collectExternalForces(
			thisNode, block, points, velocities, masses, dT, forces
		);mser;
	}	


	vl = velocities.length();
	al = accelerations.length();
	fl = forces.length();
	// rl = ratPP.length();
	cl = colors.length();
	rdl = radii.length();
	agl = ages.length();
	idl = chainIds.length();

	bool vel_ok = (vl==pl);
	bool acc_ok = (al==pl);
	bool frc_ok = (fl==pl);
	bool ratePP_ok = (rl==pl);
	bool col_ok = (cl==pl);
	bool age_ok = (agl==pl);
	bool rad_ok = (rdl==pl);
	bool id_ok = (idl==pl);


	double doubleCount ;

	for (int i=0;i<pl;i++){
		doubleCount = 0.0;
		MVector &p = points[i];
		MVector newVec = dirV;
		MVector inheritedVec = MVector::zero;
		MVector &v = velocities[i];

		// calculate rate
		//////////////////////////////////

		if (velocityRate) {
			float  vlen = velocityRateRemap.evaluate(float(v.length()));
			doubleCount += ( double(vlen) * velocityRate  );
		}
		if (inheritVelocity) inheritedVec += (v * inheritVelocity);


		if (frc_ok){
			MVector &f = forces[i];

			if (forceRate) {
				float flen = forceRateRemap.evaluate(float(f.length()));
				doubleCount += (  double(flen) * forceRate  );

			}
			if (inheritForce) inheritedVec += (f * inheritForce);
		}


		double dot=1.0;
		if (acc_ok) {
			MVector &a = accelerations[i];

			if (accelerationRate) {
				float alen = accelerationRateRemap.evaluate(float(a.length()));
				doubleCount += (  double(alen) * accelerationRate  );
			}
			if (inheritAcceleration) inheritedVec += (a * inheritAcceleration);

			if (fabs(v.length())>EPSILON && fabs(a.length())>EPSILON) {
				dot = double(facingLookup.evaluate( float(  a.normal()*v.normal()) )   );
			} else {
				dot = double(facingLookup.evaluate(0.0f ));
			}
			if (dot <= 0.0)  dot=0.0;	
		}


		// if (ratePP_ok && useRatePP) {
		// 	// doubleCount *= ratePPRemap.evaluate( ratePP[i]);
		// 	doubleCount += ratePP[i];
		// }

		doubleCount =doubleCount*rate*dot;
		doubleCount = (doubleCount < 0.0) ? 0.0 : doubleCount;

		// const int intCount = randomDistrib(
		// 	doubleCount, p, p-(v * dT.as(MTime::kSeconds)), outPos, outTim);

		MVector dirVec(dirV);

 		double dt = dT.as(MTime::kSeconds);
		const int intCount = distributeOverTime(
			doubleCount, rateCap, dt,
			p, v , 
			inheritedVec, dirVec, speed, speedRandom, spread,
			outPos, outVel, outTim
		);


		if (col_ok) {					
			for(int j=0;j < intCount;j++){
				outCol.append(colors[i]);
			}
		}
		if (age_ok) {					
			for(int j=0;j < intCount;j++){
				outAge.append(ages[i]);
			}
		}

		if (rad_ok) {					
			for(int j=0;j < intCount;j++){
				outRad.append(radii[i]);
			}
		}

		if (id_ok) {					
			for(int j=0;j < intCount;j++){
				outChainId.append(chainIds[i]);
			}
		}
			



		// MVectorArray newVels(intCount, MVector::zero);  
		// newVec = (dirV * speed) + inheritedVec;
		// // if (!speedRandom) newVec *= speed;

		// if (spread) {
		// 	spreadVectors(newVec,spread, newVels);
		// } else {
		// 	for(int j=0;j < intCount;j++){
		// 		newVels.set(newVec,j);
		// 	}
		// }

		// if (speedRandom) {
		// 	randSpeeds(speedRandom, newVels);
		// }

		// int j  = intCount;
		// for (;j;j--) outVel.append(newVels[(j-1)]);
	}



	////////////////////////////////////////////////
	////////////////////////////////////////////////
		
	hOut.set( dOutput );
	block.setClean( plug );

	return( MS::kSuccess );
}





MStatus chainArrayEmitter::initialize()

{
	MStatus st;
	//   unsigned int counter = 0;
	MString method("chainArrayEmitter::initialize");

	MFnTypedAttribute tAttr;
	MFnMessageAttribute mAttr;
	MFnNumericAttribute nAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;

	aSamplePoints = tAttr.create("samplePoints", "spts", MFnData::kVectorArray,MObject::kNullObj, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleVelocities = tAttr.create("sampleVelocities", "svls", MFnData::kVectorArray,MObject::kNullObj, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleMasses = tAttr.create("sampleMasses", "smss", MFnData::kDoubleArray,MObject::kNullObj, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleDeltaTime = uAttr.create( "sampleDeltaTime", "sdt", MFnUnitAttribute::kTime, 0.0, &st ); mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleFieldData = cAttr.create("sampleFieldData","sfd");
	cAttr.addChild(aSamplePoints);
	cAttr.addChild(aSampleVelocities);
	cAttr.addChild(aSampleMasses);
	cAttr.addChild(aSampleDeltaTime);

	st = addAttribute(aSampleFieldData);mser;

	aForces = tAttr.create("forces", "frc", MFnData::kVectorArray,MObject::kNullObj, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	st = addAttribute( aForces ); mser;
	
	aInPoints = tAttr.create("inPoints", "ipnt", MFnData::kVectorArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);

	aInVelocities = tAttr.create("inVelocities", "ivel", MFnData::kVectorArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);

	aInAccelerations = tAttr.create("inAccelerations", "iacc", MFnData::kVectorArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);


	aInRGB = tAttr.create("inRGB", "irgb", MFnData::kVectorArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);

	aInRadius = tAttr.create("inRadius", "irad", MFnData::kDoubleArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);

	aInAge = tAttr.create("inAge", "iage", MFnData::kDoubleArray);
	tAttr.setReadable(false);
	tAttr.setStorable(false);

    aInChainId = tAttr.create("inChainId", "icid", MFnData::kDoubleArray);
    tAttr.setReadable(false);
    tAttr.setStorable(false);

	aInData = cAttr.create("inData","idt");
	cAttr.addChild(aInPoints);
	cAttr.addChild(aInVelocities);
	cAttr.addChild(aInAccelerations);
	cAttr.addChild(aInRGB);
	cAttr.addChild(aInRadius);
	cAttr.addChild(aInAge);
	cAttr.addChild(aInChainId);
	cAttr.setReadable(false);
	st = addAttribute(aInData); mser;		

	aForceRateRemap =  nAttr.create( "forceRateRemap", "fcrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aForceRateRemap);mser;


	aVelocityRateRemap =  nAttr.create( "velocityRateRemap", "vlrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aVelocityRateRemap);mser;

	aAccelerationRateRemap =  nAttr.create( "accelerationRateRemap", "acrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aAccelerationRateRemap);mser;
	

	aAccelerationRate =  nAttr.create( "accelerationRate", "accr", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true); 
	nAttr.setStorable(true);
	st =	addAttribute(aAccelerationRate);	mser;

	aVelocityRate=  nAttr.create( "velocityRate", "velr", MFnNumericData::kDouble, 1.0, &st ); mser;
	nAttr.setKeyable(true); 
	nAttr.setStorable(true);
	st =	addAttribute(aVelocityRate);	mser;

	aForceRate =  nAttr.create( "forceRate", "frcr", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true); 
	nAttr.setStorable(true);
	st =	addAttribute(aForceRate);	mser;


	aRateCap= nAttr.create("rateCap","rcp", MFnNumericData::kLong);
	nAttr.setHidden( false );
	nAttr.setKeyable( true );
	nAttr.setDefault( 1 );
	st = addAttribute( aRateCap );mser;


	aInheritAcceleration =  nAttr.create( "inheritAcceleration", "iha", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true); 
	nAttr.setStorable(true);
	st =	addAttribute(aInheritAcceleration);	mser;

	aInheritVelocity =  nAttr.create( "inheritVelocity", "ihv", MFnNumericData::kDouble, 0.2, &st ); mser;
	nAttr.setKeyable(true); 
	nAttr.setStorable(true);
	st =	addAttribute(aInheritVelocity);	mser;

	aInheritForce =  nAttr.create( "inheritForce", "ihf", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true); 
	nAttr.setStorable(true);
	st =	addAttribute(aInheritForce);mser;

	aFacingRemap =  nAttr.create( "facingRemap", "fcr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aFacingRemap);mser;

	attributeAffects(aInPoints,aSamplePoints);
	attributeAffects(aInVelocities,aSampleVelocities);
	attributeAffects(aInPoints,aSampleMasses);
	attributeAffects(aInPoints,aSampleFieldData);

	return( MS::kSuccess );
}


