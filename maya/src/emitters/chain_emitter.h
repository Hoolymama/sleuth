
#ifndef _CHAINEMITTER
#define _CHAINEMITTER

#include <maya/MIOStream.h>
#include <maya/MTime.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MPxEmitterNode.h>
#include <maya/MFloatVectorArray.h>

#include "lookup.h"
#include "errorMacros.h"


class chainEmitter: public MPxEmitterNode
{
public:
	chainEmitter();
	virtual ~chainEmitter();

	
	
	
	static void		*creator();
	static MStatus	initialize();
	virtual MStatus	compute( const MPlug& plug, MDataBlock& block );
	
	
	// geometry samples output to field
	static MObject aSamplePoints;
	static MObject aSampleVelocities;
	static MObject aSampleMasses;
	static MObject aSampleDeltaTime;
	static MObject aSampleFieldData;
	// forces returned at the triangle centres
	static MObject aForces;

	static MObject aForceRate;
	static MObject aVelocityRate;
	static MObject aAccelerationRate;
	static MObject aRateCap;
	static MObject aForceRateRemap;
	static MObject aVelocityRateRemap;
	static MObject aAccelerationRateRemap;
	
	static MObject aForceFacingCurve;
	static MObject aVelocityFacingCurve;
	static MObject aAccelerationFacingCurve;
	
	static MObject aComponentCalculation;
		
	//static MObject aRateMap;
	
	static MObject aInheritForce;
	static MObject aInheritVelocity;
	static MObject aInheritAcceleration;
	static MObject aSweepTimeOffset;

	// static MObject aEmitFromCenter;


	static MTypeId	id;
	
	MStatus getLookup(const MObject &object, MObject &attr, lookup & result);

private:
	//double  areaFn(const MVector &p1,const MVector &p2,const MVector &p3);
	enum CompCalc { kFpApV, kFmAmV, kFmApV, kAmFpV , kVmFpA, kFpAmV, kApFmV , kVpFmA };
	
//	unsigned int randCount(double f);

	// void distributeOverTime(
	// 	unsigned tri,
	// 	const double &newRate,
	// 	const double &oldRate,
	// 	const MVector &newP1,
	// 	const MVector &newP2,
	// 	const MVector &newP3,
	// 	const MVector &oldP1,
	// 	const MVector &oldP2,
	// 	const MVector &oldP3,
	// 	const MVector & newVec,
	// 	const MVector & oldVec,
	// 	const double &speedRandom,	
	// 	const double &spread,	
	// 	const double &dt,	
	// 	const double &sweepTimeOffset,
	// 	MVectorArray &pos,
	// 	MVectorArray &vel,
	// 	MDoubleArray &tim,
	// 	MDoubleArray &triId
	// );
	
	// void  distributeOverTime(
	// 	unsigned tri,
	// 	const double &newRate,
	// 	const double &oldRate,
	// 	const MVector &newC,
	// 	const MVector &oldC,
	// 	const MVector & newVec,
	// 	const MVector & oldVec,
	// 	const double &speedRandom,	
	// 	const double &spread,	
	// 	const double &dt,	
	// 	const double &sweepTimeOffset,
	// 	MVectorArray &pos,
	// 	MVectorArray &vel,
	// 	MDoubleArray &tim,
	// 	MDoubleArray &triId
	// );
void distributeOverTime(
	unsigned tri,
	const double &rate,
	int rateCap,
	const MVector &newC,
	const MVector &oldC,
	const MVector & newVec,
	const MVector & oldVec,
	const double &speedRandom,	
	const double &spread,	
	const double &dt,	
	const double &sweepTimeOffset,
	MVectorArray &pos,
	MVectorArray &vel,
	MDoubleArray &tim,
	MDoubleArray &triId
	) ;


	double	doubleValue( MDataBlock& block, MObject &att );
	MVector	vectorValue( MDataBlock& block, MObject &att );
	bool	isFullValue( int plugIndex, MDataBlock& block );
	long	seedValue( int plugIndex, MDataBlock& block );
	double	inheritFactorValue( int plugIndex, MDataBlock& block );

	MTime	currentTimeValue( MDataBlock& block );
	MTime	startTimeValue( int plugIndex, MDataBlock& block );
	MTime	deltaTimeValue( int plugIndex, MDataBlock& block );

	// for calculating triangle acceleration
	MVectorArray m_lastEmissionVel;
	MVectorArray m_lastEmissionPos;	
	
	unsigned m_lastTriangleCount;
	MVectorArray m_lastVelocity;

	//bool m_triangleCountChanged;

};

// inlines
//
inline double chainEmitter::doubleValue( MDataBlock& block , MObject &att)
{
	MStatus status;

	MDataHandle hValue = block.inputValue( att, &status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


inline MVector chainEmitter::vectorValue( MDataBlock& block,MObject &att )
{
	MStatus status;
	MVector valueV(0.0, 0.0, 0.0);

	MDataHandle hValue = block.inputValue( att, &status );

	if( status == MS::kSuccess )
	{
		double3 &value = hValue.asDouble3();

		valueV[0] = value[0];
		valueV[1] = value[1];
		valueV[2] = value[2];
	}

	return(valueV );
}

inline bool chainEmitter::isFullValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	bool value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mIsFull, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asBool();
		}
	}

	return( value );
}
inline long chainEmitter::seedValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	long value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mSeed, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asInt();
				//cerr << value << endl;
		}
	}

	return( value );
}

inline double chainEmitter::inheritFactorValue(int plugIndex,MDataBlock& block)
{
	MStatus status;
	double value = 0.0;

	MArrayDataHandle mhValue = block.inputArrayValue( mInheritFactor, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asDouble();
		}
	}

	return( value );
}

inline MTime chainEmitter::currentTimeValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mCurrentTime, &status );

	MTime value(0.0);
	if( status == MS::kSuccess )
		value = hValue.asTime();

	return( value );
}

inline MTime chainEmitter::startTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mStartTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}

	return( value );
}

inline MTime chainEmitter::deltaTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mDeltaTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}
	return( value );
}
#endif
