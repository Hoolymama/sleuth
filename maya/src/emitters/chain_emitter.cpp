

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MDataHandle.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MPlugArray.h>
#include <maya/MRenderUtil.h> 
#include <maya/MQuaternion.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MArrayDataBuilder.h>

#include "mayaFX.h"
#include "mayaMath.h"
#include "attrUtils.h"
#include "jMayaIds.h"
#include "chain_emitter.h"



const double oneThird = (1.0 / 3.0);

MObject chainEmitter::aSamplePoints;
MObject chainEmitter::aSampleVelocities;
MObject chainEmitter::aSampleMasses;
MObject chainEmitter::aSampleDeltaTime;
MObject chainEmitter::aSampleFieldData;

MObject chainEmitter::aForces;

// MObject chainEmitter::aEmitFromCenter;


MObject chainEmitter::aForceRate;
MObject chainEmitter::aVelocityRate;
MObject chainEmitter::aAccelerationRate;
MObject chainEmitter::aRateCap;
MObject chainEmitter::aForceRateRemap;
MObject chainEmitter::aVelocityRateRemap;
MObject chainEmitter::aAccelerationRateRemap;

MObject chainEmitter::aForceFacingCurve;
MObject chainEmitter::aVelocityFacingCurve;
MObject chainEmitter::aAccelerationFacingCurve;

MObject chainEmitter::aComponentCalculation;
	

MObject chainEmitter::aInheritForce;
MObject chainEmitter::aInheritVelocity;
MObject chainEmitter::aInheritAcceleration;
MObject chainEmitter::aSweepTimeOffset;


MTypeId chainEmitter::id( k_chainEmitter );


chainEmitter::chainEmitter():m_lastTriangleCount(0)
{
}


chainEmitter::~chainEmitter()
{
}


void *chainEmitter::creator()
{
	return new chainEmitter;
}

void  chainEmitter::distributeOverTime(
	unsigned tri,
	const double &rate,
	int rateCap,
	const MVector &newPos,
	const MVector &oldPos,
	const MVector & newVel,
	const MVector & oldVel,
	const double &speedRandom,	
	const double &spread,	
	const double &dt,	
	const double &sweepTimeOffset,
	MVectorArray &pos,
	MVectorArray &vel,
	MDoubleArray &tim,
	MDoubleArray &triId
	) {

	const double  PI  = 3.1415927;
	const double  _2PI = 2.0 * PI;
	
	// stuff needed for spread 
	//////////////////////////////////
	double rot_min = cos(spread*PI);
	double rot_t, rot_w, rot_z;
	double rot_range = 1.0 - rot_min;
	//////////////////////////////////
	 
	double s, t, oneMinusSweep;
	MVector sweepPos;


	//double highRate = (newRate > oldRate) ? newRate : oldRate;
	// double deltaRate = newRate - oldRate;
	MVector deltaVel(newVel - oldVel);
	MVector deltaPos(newPos - oldPos);
	// cerr << "Old New Delta Vec" << oldVel << " " << newVel << " " << deltaVel << endl;
	//double newRateRecip = newRate / 1.0;
	unsigned int nRate = mayaMath::randCount(rate);
	if (nRate > rateCap) nRate=rateCap;
	
	//cerr << "------------" << endl;
	for(unsigned i = 0; i < nRate; ++i)
	{
		
		double sweep = (i+0.5) / double(nRate);		

		MVector sweepPos = oldPos+ (deltaPos * sweep);

		MVector sweepVel = oldVel + (deltaVel * sweep );


		
		double mag = sweepVel.length();
		if (speedRandom > 0.0) {
			mag = mag *  (1.0 + (speedRandom * 2.0 * (drand48() -0.5)));
		}
		
    	// MVector rot_v;
		if (spread > 0.0) {
			MVector tmp;
			MQuaternion q(MVector::zAxis, sweepVel);
			rot_z = (drand48() * rot_range) + rot_min;
			rot_t = drand48() * _2PI;
			rot_w = (sqrt( 1.0 - rot_z*rot_z )) ; // * mag;
			tmp.z = rot_z ; // * mag;
			tmp.x = rot_w * cos( rot_t );
			tmp.y = rot_w * sin( rot_t );
			sweepVel = tmp.rotateBy(q);
		}	else {
			sweepVel.normalize();
		}
		sweepVel = sweepVel * mag;


		// particles that were emitted at the beginning of the frame will have travelled a bit by now
		//MVector posOffset = ((vec * dt) * (1.0-(sweep - sweepTimeOffset)) );


		//MVector posOffset = vec  * (1.0 - sweep);
		// MVector posOffset = MVector::zero;
		// pos.append( sweepPos + posOffset) ;	

		vel.append(sweepVel);
		pos.append( sweepPos + ((1.0 - sweep) * dt * sweepVel)) ;	
		tim.append( sweep);
		triId.append(tri);
	}
	///cerr << "------------" << endl;

}


MStatus chainEmitter::initialize()

{
	MStatus st;
//  unsigned int counter = 0;
	MString method("chainEmitter::initialize");

	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;
	MFnEnumAttribute eAttr;
	MFnNumericAttribute nAttr;


	// aEmitFromCenter =  nAttr.create( "emitFromCenter", "emc", MFnNumericData::kBoolean );
	// nAttr.setKeyable(true);
	// nAttr.setStorable(true);
	// nAttr.setDefault(false);
	// st = addAttribute(aEmitFromCenter);mser;


	aForceRate =  nAttr.create( "forceRate", "fcr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aForceRate);mser;



	aVelocityRate =  nAttr.create( "velocityRate", "vlr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aVelocityRate);mser;

	aAccelerationRate =  nAttr.create( "accelerationRate", "acr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aAccelerationRate);mser;
	;
	aRateCap= nAttr.create("rateCap","rcp", MFnNumericData::kLong);
	nAttr.setHidden( false );
	nAttr.setKeyable( true );
	nAttr.setDefault( 1 );
	st = addAttribute( aRateCap );mser;


	aForceRateRemap =  nAttr.create( "forceRateRemap", "fcrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aForceRateRemap);mser;


	aVelocityRateRemap =  nAttr.create( "velocityRateRemap", "vlrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aVelocityRateRemap);mser;

	aAccelerationRateRemap =  nAttr.create( "accelerationRateRemap", "acrr", MFnNumericData::kDouble );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aAccelerationRateRemap);mser;
	;


// 	enum CompCalc { kFpApV, kFmAmV, kFmApV, kAmFpV , kVmFpA, kFpAmV, kApFmV , kVpFmA };
	
	
	aComponentCalculation = eAttr.create("rateCalculation", "rcl");
	eAttr.addField("Frc + Acc + Vel", kFpApV);
	eAttr.addField("Frc * Acc * Vel", kFmAmV);
	eAttr.addField("Frc * (Acc + Vel)", kFmApV);
	eAttr.addField("Acc * (Frc + Vel)", kAmFpV);
	eAttr.addField("Vel * (Frc + Acc)", kVmFpA);
	eAttr.addField("Frc + (Acc * Vel)", kFpAmV);
	eAttr.addField("Acc + (Frc * Vel)", kApFmV);
	eAttr.addField("Vel + (Frc * Acc)", kVpFmA);

	eAttr.setDefault(kFpApV);
	eAttr.setKeyable(true);
	eAttr.setWritable(true);
	st = addAttribute(aComponentCalculation );mser;





	aInheritForce =  nAttr.create( "inheritForce", "ifc", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritForce);	mser;

	aInheritVelocity =  nAttr.create( "inheritVelocity", "ivl", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritVelocity);	mser;

	aInheritAcceleration =  nAttr.create( "inheritAcceleration", "iac", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aInheritAcceleration);	mser;

	aSweepTimeOffset =  nAttr.create( "sweepTimeOffset", "sto", MFnNumericData::kDouble, 0.0, &st ); mser;
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st =	addAttribute(aSweepTimeOffset);	mser;



	aForceFacingCurve	= nAttr.create("forceFacingCurve","ffc", MFnNumericData::kDouble, 1.0, &st); mser;
	nAttr.setHidden(false);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	st = addAttribute(aForceFacingCurve); mser;

	aVelocityFacingCurve = nAttr.create("velocityFacingCurve","vfc", MFnNumericData::kDouble, 1.0, &st); mser;
	nAttr.setHidden(false);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	st = addAttribute(aVelocityFacingCurve); mser;

	aAccelerationFacingCurve = nAttr.create("accelerationFacingCurve","acfc", MFnNumericData::kDouble, 1.0, &st); mser;
	nAttr.setHidden(false);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setWritable(true);
	st = addAttribute(aAccelerationFacingCurve); mser;

	aSamplePoints = tAttr.create("samplePoints", "spts", MFnData::kVectorArray,MObject::kNullObj, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleVelocities = tAttr.create("sampleVelocities", "svls", MFnData::kVectorArray,MObject::kNullObj, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleMasses = tAttr.create("sampleMasses", "smss", MFnData::kDoubleArray,MObject::kNullObj, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleDeltaTime = uAttr.create( "sampleDeltaTime", "sdt", MFnUnitAttribute::kTime, 0.0, &st ); mser;
	tAttr.setStorable(false);
	tAttr.setReadable(true);

	aSampleFieldData = cAttr.create("sampleFieldData","sfd");
	cAttr.addChild(aSamplePoints);
	cAttr.addChild(aSampleVelocities);
	cAttr.addChild(aSampleMasses);
	cAttr.addChild(aSampleDeltaTime);

	//st = addAttribute(aSamplePoints );mser;
//st = addAttribute(aSampleVelocities );mser;
//st = addAttribute(aSampleMasses );mser;
	//st = addAttribute(aSampleDeltaTime);mser;
	st = addAttribute(aSampleFieldData);mser;


	aForces = tAttr.create("forces", "frc", MFnData::kVectorArray,MObject::kNullObj, &st);mser;
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	tAttr.setArray(true);
	st = addAttribute( aForces ); mser;

	attributeAffects(mCurrentTime,aSamplePoints);
	attributeAffects(mCurrentTime,aSampleVelocities);
	attributeAffects(mCurrentTime,aSampleMasses);
	attributeAffects(mCurrentTime,aSampleFieldData);

	return( MS::kSuccess );
}


MStatus chainEmitter::getLookup(const MObject &node, MObject &attr, lookup & result){
	MStatus st;
	MFnAnimCurve aniFn; 
	st = getAniCurveFn(node, attr, aniFn);
	if (!(st.error())) 	result.create(aniFn, 20);
	return st;
}
	

MStatus chainEmitter::compute(const MPlug& plug, MDataBlock& block)
{
	

	MStatus st;
	MString method("chainEmitter::compute");
	if( !(plug == mOutput) ) return( MS::kUnknownParameter );
	
	MObject thisNode = thisMObject();
	MFnDependencyNode thisNodeFn(thisNode);

	MObject  aSpread = thisNodeFn.attribute(MString("spread"));
	MObject  aNormalSpeed = thisNodeFn.attribute(MString("normalSpeed"));
	MObject  aSpeedRandom = thisNodeFn.attribute(MString("speedRandom"));
	// MObject  aScaleRate = thisNodeFn.attribute(MString("scaleRateByObjectSize"));
	MObject  aEnableTextureRate = thisNodeFn.attribute(MString("enableTextureRate"));

	// set up output at multiindex
	//////////////////////////////////////////////////////////
	int multiIndex = plug.logicalIndex( &st);mser;
	MArrayDataHandle hOutArray = block.outputArrayValue(mOutput, &st);mser;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);mser;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);mser;
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &st );mser;
	//////////////////////////////////////////////////////////

	// position and velocity arrays to append new particle data.
	//////////////////////////////////////////////////////////
	MVectorArray outPos = fnOutput.vectorArray("position", &st);mser;
	MVectorArray outVel = fnOutput.vectorArray("velocity", &st);mser;
	MDoubleArray outTim = fnOutput.doubleArray("timeInStep", &st);mser;
	MDoubleArray outTrianglId = fnOutput.doubleArray("triangleId", &st);mser;
	//////////////////////////////////////////////////////////


	// check time and isFull
	//////////////////////////////////////////////////////////
	bool beenFull = isFullValue( multiIndex, block );
	if( beenFull ) return( MS::kSuccess );
	MTime cT = currentTimeValue( block );
	MTime sT = startTimeValue( multiIndex, block );
	MTime dT = deltaTimeValue( multiIndex, block );
	double dt =  dT.as(MTime::kSeconds);
	double dtRecip = 1.0 / dt;
	//////////////////////////////////////////////////////////
	


	// Bail / reset if going backwards
	//////////////////////////////////////////////////////////
	if( (cT <= sT) || (dT <= 0.0) )
	{
		m_lastEmissionPos.clear();
		m_lastEmissionVel.clear();
		hOut.set( dOutput );
		block.setClean( plug );
		return( MS::kSuccess );
	}
	//////////////////////////////////////////////////////////

	// Get the swept geometry data
	//////////////////////////////////////////////////////////
	MObject thisObj = this->thisMObject();
	MPlug sweptPlug( thisObj, mSweptGeometry );
	if (! sweptPlug.isConnected() ) {
		hOut.set( dOutput );
		block.setClean( plug );
		return( MS::kSuccess );
	}
	//////////////////////////////////////////////////////////



	// get triangle data for sampling purposes
	// Note, we have to access the swept geometry again later to
	// get vertex positins and UVs
	//////////////////////////////////////////////////////////
	MDataHandle sweptHandle = block.inputValue( mSweptGeometry );
	MObject sweptData = sweptHandle.data();
	MVectorArray centers0,  centers1, velocities, accelerations, forces;
	// MDoubleArray areas;
	// double totalArea = 0;
	//////////////////////////////////////////////////////////
	
	
	MFloatArray uCoords;
	MFloatArray vCoords;
	MFloatPointArray samplePoints;
	MFloatVectorArray sampleNormals;


	bool useTexRate = block.inputValue(aEnableTextureRate).asBool();
	bool triangleCountChanged ;
	if (useTexRate) {
		 triangleCountChanged = mayaFX::getDynTriangleArrays( 
		 	dtRecip, m_lastVelocity, m_lastTriangleCount,	
		 	sweptData, centers0,  centers1,  velocities, accelerations, 
		 	uCoords,vCoords,samplePoints,sampleNormals);
	} else {
		 triangleCountChanged = mayaFX::getDynTriangleArrays( 
		 	dtRecip, m_lastVelocity, m_lastTriangleCount,	
		 	sweptData, centers0,  centers1,  velocities, accelerations);
	}

	unsigned numTriangles = centers1.length();

	m_lastTriangleCount = numTriangles;
	m_lastVelocity.copy(velocities);

	if (! numTriangles) return( MS::kSuccess );

	// if (triangleCountChanged) {
	// 	// m_lastEmissionRate = 	MDoubleArray(numTriangles);
	// 	m_lastEmissionVec = MVectorArray(numTriangles);
	// }
	//////////////////////////////////////////////////////////
	
	//	cerr << "here 1" << endl;
	// Get ani curve lookups
	//////////////////////////////////////////////////////////
	lookup forceRateLut, velocityRateLut, accelerationRateLut;
	st = getLookup(thisNode, aForceFacingCurve,forceRateLut);mser;
	st = getLookup(thisNode, aVelocityFacingCurve,velocityRateLut);mser;
	st = getLookup(thisNode, aAccelerationFacingCurve,accelerationRateLut);mser;
	
	lookup forceRateRemap, velocityRateRemap, accelerationRateRemap;
	st = getLookup(thisNode, aForceRateRemap,forceRateRemap);mser;
	st = getLookup(thisNode, aVelocityRateRemap,velocityRateRemap);mser;
	st = getLookup(thisNode, aAccelerationRateRemap,accelerationRateRemap);mser;
	//////////////////////////////////////////////////////////
	

	// Get rate values
	//////////////////////////////////////////////////////////
	double theRate =  block.inputValue( mRate ).asDouble() ;
	if (theRate < 0.0) theRate = 0.0;
	double forceRate =  block.inputValue( aForceRate ).asDouble() ;
	double velocityRate =  block.inputValue( aVelocityRate ).asDouble() ;
	double accelerationRate =  block.inputValue( aAccelerationRate ).asDouble() ;
	CompCalc calc = (CompCalc)block.inputValue(aComponentCalculation).asShort();
	// bool scaleRate = block.inputValue(aScaleRate).asBool(); 
	// if (!scaleRate) theRate = theRate / totalArea;	
	//////////////////////////////////////////////////////////	

	int rateCap = int(block.inputValue(aRateCap).asInt());


	// is the rate mapped ?
	//////////////////////////////////////////////////////////
	MFloatArray texturedRates;
	if (useTexRate) {
	MObject  aTextureRate = thisNodeFn.attribute(MString("textureRate"));
		useTexRate =  mayaFX::sampleTexture(thisNode,aTextureRate,uCoords,vCoords,samplePoints,sampleNormals,texturedRates);
	}
	//////////////////////////////////////////////////////////

//	cerr << "here 4" << endl;

	// are forces blowing on the surface?
	//cerr << "dT " << dT << endl; 
	//////////////////////////////////////////////////////////	
	MDoubleArray masses(numTriangles, 1.0);	
	st = mayaFX::collectExternalForces(thisNode, block, centers1, velocities, masses, dT, forces);mser;
	//////////////////////////////////////////////////////////	
//	cerr << "here 5" << endl;

	//cerr << "forces " <<  forces << endl;;


	// Get attributes.
	//////////////////////////////////////////////////////////
	long seedVal = seedValue( multiIndex, block );
	double speed = doubleValue( block, mSpeed );
	double speedRandom = doubleValue( block, aSpeedRandom );
	double normalSpeed = doubleValue( block, aNormalSpeed );
	double spread = doubleValue( block, aSpread );
	double inheritForce = doubleValue( block, aInheritForce );
	double inheritVelocity = doubleValue( block, aInheritVelocity );
	double inheritAcceleration = doubleValue( block, aInheritAcceleration );
	double sweepTimeOffset = doubleValue( block, aSweepTimeOffset );


	if (spread > 1 ) spread = 1;
	if (spread < 0 ) spread = 0;
	MVector dirV = vectorValue( block, mDirection );

	srand48(seedVal);

	bool lastVelValid = (numTriangles == m_lastEmissionVel.length());

	MVectorArray stashedEmissionVel(numTriangles);
	//MVectorArray stashedEmissionPos(numTriangles);
	
	for (unsigned t=0; t < numTriangles; t++ )
	{
		MFnDynSweptGeometryData fnSweptData( sweptData );
		MDynSweptTriangle tri = fnSweptData.sweptTriangle( t );


		MVector N = tri.normal();
//		cerr << "here 6" << endl;
		float velocityFacingMult = 1.0f;
		double velocityMag = velocities[t].length();
		MVector velocityNorm;
		if (velocityMag > 0.0) {
			velocityNorm = velocities[t] / velocityMag;
			if (!velocityRateLut.isConstantOne()) {
				float dot = float(velocityNorm*N);
				velocityFacingMult = velocityRateLut.evaluate(dot);
			}

		}
	//	cerr << "here 7" << endl;

		float forceFacingMult = 1.0f;
		double forceMag = forces[t].length();
		MVector forceNorm;
		if (forceMag > 0.0) {
			forceNorm = forces[t] / forceMag;
			if (!forceRateLut.isConstantOne()) {
				float dot = float(forceNorm*N);
				forceFacingMult = forceRateLut.evaluate(dot);
				//cerr << "forceFacingMult = " << forceFacingMult  << endl;
			} else {
				//cerr << "is constant one" << endl;
			}
		}
	//	cerr << "here 8" << endl;

		float accelFacingMult = 1.0f;
		double accelMag = accelerations[t].length();
		MVector accelNorm;
		if (accelMag > 0.0) {
			accelNorm = accelerations[t] / accelMag;
			if (!accelerationRateLut.isConstantOne()) {
				float dot = float(accelNorm*velocityNorm);
				accelFacingMult = accelerationRateLut.evaluate(dot);
			}
		}
		//cerr << "here 9" << endl;

		if (!forceRateRemap.isConstantOne()) forceMag = forceRateRemap.evaluate(float(forceMag));
		if (!velocityRateRemap.isConstantOne()) velocityMag = velocityRateRemap.evaluate(float(velocityMag));
		if (!accelerationRateRemap.isConstantOne()) accelMag = accelerationRateRemap.evaluate(float(accelMag));

		//cerr << "here 10" << endl;

		// calculate this rate.
		
		
		double thisRate ;
		double fr = (forceRate * forceMag * forceFacingMult) ; 
		// cerr <<  "fr is: " << fr << endl;
		double vr = (velocityRate * velocityMag * velocityFacingMult) ; 
		double ar = (accelerationRate * accelMag * accelFacingMult);
			
				
		switch ( calc ) {
			case chainEmitter::kFpApV:
			thisRate = fr + ar + vr; 
			break;
			case chainEmitter::kFmAmV:
			thisRate = fr * ar * vr; 
			break;
			case chainEmitter::kFmApV:
			thisRate = fr * (ar + vr); 
			break;
			case chainEmitter::kAmFpV:
			thisRate =  ar * (fr + vr); 
			break;
			case chainEmitter::kVmFpA:
			thisRate =  vr * (fr + ar); 
			break;
			case chainEmitter::kFpAmV:
			thisRate = fr + (ar * vr); 
			break;
			case chainEmitter::kApFmV:
			thisRate = ar + (fr * vr); 
			break;
			case chainEmitter::kVpFmA:
			thisRate = vr + (fr * ar); 
			break;			
			
			default:
			thisRate = fr + ar + vr; 
			break;
		}			


		thisRate = thisRate * theRate * dt; // mult by dt so that oversampling works

	//	cerr << dt << endl;
		if (useTexRate) thisRate = thisRate * double(texturedRates[t]);

		if (thisRate < 0.0) thisRate = 0.0;

		MVector inheritVec(MVector::zero);
		MVector normalVec(MVector::zero);


		if (inheritVelocity) inheritVec = (inheritVelocity * velocities[t] ) ;
		// cerr << "inheritVec " << inheritVec << endl;

		if (inheritForce) inheritVec = inheritVec + (inheritForce * forces[t] ) ;
		//cerr << "inheritForce:" << inheritForce << endl;


		if (inheritAcceleration) inheritVec = inheritVec + (inheritAcceleration * accelerations[t] ) ;

		MVector newVel(MVector::zero);
		MVector dirVec(dirV);
		if (normalSpeed) normalVec = (normalSpeed * N);
		newVel = (dirVec + normalVec + inheritVec ) * speed;
		// cerr << "newVel " << newVel << endl;

		stashedEmissionVel.set(newVel,t) ;

		// MVectorArray v0(3);
		// MVectorArray v1(3);
		// v0[0] = tri.vertex( 0 ,0.0);
		// v0[1] = tri.vertex( 1 ,0.0);
		// v0[2] = tri.vertex( 2 ,0.0);
		// v1[0] = tri.vertex( 0 ,1.0);
		// v1[1] = tri.vertex( 1 ,1.0);
		// v1[2] = tri.vertex( 2 ,1.0);



		const MVector & c1 = centers1[t];
		const MVector & c0 = centers0[t];

		MVector lastVel;
		if (lastVelValid) {
			lastVel = m_lastEmissionVel[t];
		} else {
			lastVel = newVel;
		}

		distributeOverTime(	
			t,
			thisRate, rateCap,
			c1,c0,
			newVel,
			lastVel,
			speedRandom,
			spread ,
			dt,
			sweepTimeOffset, 
			outPos,
			outVel,
			outTim,
			outTrianglId);


	//	cerr  << "m_lastEmissionRate[t]: " << m_lastEmissionRate[t] << endl ;
	//	cerr  << "thisRate: " << thisRate << endl ;


	}




	//m_lastEmissionRate = stashedRateArray;
	m_lastEmissionVel = stashedEmissionVel;
	// m_lastEmissionPos = stashedEmissionPos;

	hOut.set( dOutput );
	block.setClean( plug );
	return( MS::kSuccess );
}


