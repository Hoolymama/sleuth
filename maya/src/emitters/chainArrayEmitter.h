
#include <maya/MIOStream.h>
#include <maya/MTime.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MPxEmitterNode.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MPlugArray.h>
#include "errorMacros.h"


class chainArrayEmitter: public MPxEmitterNode
/** @dia:pos -22,-53.1 */
{
public:
	chainArrayEmitter();
	virtual ~chainArrayEmitter();

	
	
	static void		*creator();
	static MStatus	initialize();
	virtual MStatus	compute( const MPlug& plug, MDataBlock& block );
	
	// geometry samples output to field
	static MObject aSamplePoints;
	static MObject aSampleVelocities;
	static MObject aSampleMasses;
	static MObject aSampleDeltaTime;
	static MObject aSampleFieldData;
	static MObject aForces;

	static MObject aInPoints;
	static MObject aInVelocities;
	static MObject aInAccelerations;

	static MObject aInRGB;
	static MObject aInRadius;
	static MObject aInAge;
	static MObject aInChainId;
	static MObject aInData;

	static MObject aForceRateRemap;
	static MObject aVelocityRateRemap;
	static MObject aAccelerationRateRemap;

	static MObject aAccelerationRate;
	static MObject aVelocityRate;
	static MObject aForceRate;

	static MObject aRateCap;
		
	static MObject aInheritAcceleration;
	static MObject aInheritVelocity;
	static MObject aInheritForce;

	static MObject aFacingRemap;
	
	static MTypeId	id;


private:

	// double  areaFn(const MVector &p1,const MVector &p2,const MVector &p3);

	//unsigned int randomDistrib(double f, const MVector &p1,
	//const MVector &p2,const MVector &p3,MVectorArray &result);
	
	// unsigned int randomDistrib(double f, const MPoint &p1,
	// const MPoint &p2,MVectorArray &pos,
	// MDoubleArray &tim);
	
	// void spreadVectors(const MVector & dir,double spread, MVectorArray & newVels);
	// void randSpeeds(double randomSpeed, MVectorArray & vels);
	int distributeOverTime(
			double rate, int rateCap, double dt,
			const MVector &p, const MVector &v , 
			const MVector &inheritedVec, const MVector &dirVec, 
			double speed, double speedRandom, double spread,
			MVectorArray &outPos, 
			MVectorArray &outVel, 
			MDoubleArray &outTim
	) ;

	//
	MStatus getAnimFn(const MObject &att, MFnAnimCurve& functionSet);
	double	doubleValue( MDataBlock& block, MObject &att );
	MVector	vectorValue( MDataBlock& block, MObject &att );
	bool	isFullValue( int plugIndex, MDataBlock& block );
	long	seedValue( int plugIndex, MDataBlock& block );
	double	inheritFactorValue( int plugIndex, MDataBlock& block );
	MTime	currentTimeValue( MDataBlock& block );
	MTime	startTimeValue( int plugIndex, MDataBlock& block );
	MTime	deltaTimeValue( int plugIndex, MDataBlock& block );
};
// inlines

inline long chainArrayEmitter::seedValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	long value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mSeed, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asInt();
				//cerr << value << endl;
		}
	}

	return( value );
}

inline MStatus chainArrayEmitter::getAnimFn(const MObject &att, MFnAnimCurve& functionSet) {
	
	
	MStatus st;

	MString method("chainArrayEmitter::getAnimFn");
	
	MObject aniNode;
	MPlugArray plugArray;	

	MPlug aniPlug(thisMObject(), att);
	if(aniPlug.connectedTo(plugArray,1,0)) {
		aniNode = plugArray[0].node(&st); mser;
	 } else {
		 return MS::kFailure;
	}
	st = functionSet.setObject(aniNode); msert;
	return MS::kSuccess;
}

//
inline double chainArrayEmitter::doubleValue( MDataBlock& block , MObject &att)
{
	MStatus status;

	MDataHandle hValue = block.inputValue( att, &status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


inline MVector chainArrayEmitter::vectorValue( MDataBlock& block,MObject &att )
{
	MStatus status;
	MVector valueV(0.0, 0.0, 0.0);

	MDataHandle hValue = block.inputValue( att, &status );

	if( status == MS::kSuccess )
	{
		double3 &value = hValue.asDouble3();

		valueV[0] = value[0];
		valueV[1] = value[1];
		valueV[2] = value[2];
	}

	return(valueV );
}

inline bool chainArrayEmitter::isFullValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	bool value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( mIsFull, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asBool();
		}
	}

	return( value );
}

inline double chainArrayEmitter::inheritFactorValue(int plugIndex,MDataBlock& block)
{
	MStatus status;
	double value = 0.0;

	MArrayDataHandle mhValue = block.inputArrayValue( mInheritFactor, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asDouble();
		}
	}

	return( value );
}

inline MTime chainArrayEmitter::currentTimeValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mCurrentTime, &status );

	MTime value(0.0);
	if( status == MS::kSuccess )
		value = hValue.asTime();

	return( value );
}

inline MTime chainArrayEmitter::startTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mStartTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}

	return( value );
}

inline MTime chainArrayEmitter::deltaTimeValue( int plugIndex, MDataBlock& block )
{
	MStatus status;
	MTime value(0.0);

	MArrayDataHandle mhValue = block.inputArrayValue( mDeltaTime, &status );
	if( status == MS::kSuccess )
	{
		status = mhValue.jumpToElement( plugIndex );
		if( status == MS::kSuccess )
		{
			MDataHandle hValue = mhValue.inputValue( &status );
			if( status == MS::kSuccess )
				value = hValue.asTime();
		}
	}
	return( value );
}

